const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

module.exports = {
    entry: {
        app: './src/main.js',
    },
    devtool: 'inline-source-map',
    devServer: {
        contentBase: './dist',
        compress: true,
        port: 15773
    },
    // resolve: {
    //     extensions: ['.js'],
    //     alias: {
    //         // '@': path.join( __dirname, '/src'),
    //         // '~': path.join( __dirname, '/src/resource/')
    //     }
    // },
    module: {
        rules: [{
            test: /\.scss$/,
            use: [{
                loader: "style-loader"
            }, {
                loader: "css-loader", options: {
                    sourceMap: true
                }
            }, {
                loader: "sass-loader", options: {
                    sourceMap: true
                }
            }]
        }]
    },
    plugins: [
        new CleanWebpackPlugin(['dist']),
        new HtmlWebpackPlugin({
            title: 'gobang'
        }),
        new webpack.optimize.CommonsChunkPlugin({
            name: 'js/vendor',
            children: true,
            async: true
        })
    ],
    output: {
        filename: '[name].js',
        chunkFilename: '[name].js',
        path: path.resolve( __dirname, 'dist')
    }
};