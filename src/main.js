( async () => {
    require('./scss/index.scss');
    
    let _ = await require('./lib/lodash');
    let { alphabet_encode, alphabet_decode} = await require('./components/alphabet');

    let Chess = function () {
        this.stones = false;
        this.history = [];

        let axle = [];
        for( let i = 0; i < 15; i++ ) {
            let row = [];
            for( let n = 0; n < 15; n++ ) {
                row[n] = null;
            }
            axle[i] = row;
        }
        this.chessboard =  axle;
    };
    
    Chess.prototype = {
        currentStones () {
            return this.stones? 'white': 'black';
        },
        addStep ( x, y) {
            try {
                if( _.some( this.history, { x, y})){
                    return false;
                } else {
                    let stones = this.currentStones();
                    this.chessboard[ alphabet_decode(x) - 1] [y - 1] = stones;
                    this.history.push( { x, y, stones});
                    this.stones = !this.stones;
                    let count = 0;
                    _.map( this.history, v => { 
                        if( v.stones == stones){
                            count++;
                        }
                        return v;
                    })
                    return count;
                }
            } catch (error) {
                console.error( error);
                return false;
            }
        },
        showStep () {
            return this.history;
        },
        checkWin () {
            //  获得最后一次落子
            let { x, y, stones} = _.last( this.history);
            
            console.log( x, y);
            x = Number( alphabet_decode( x));
            y = Number( y);

            let angle = {
                0: this.getCount0( x, y, stones),
                45: this.getCount45( x, y, stones),
                90: this.getCount90( x, y, stones),
                135: this.getCount135( x, y, stones),
                180: this.getCount180( x, y, stones),
                225: this.getCount225( x, y, stones),
                270: this.getCount270( x, y, stones),
                315: this.getCount315( x, y, stones)
            };
            angle._1 = angle[0] + angle[180] - 1;
            angle._2 = angle[45] + angle[225] - 1;
            angle._3 = angle[90] + angle[270] - 1;
            angle._4 = angle[135] + angle[315] - 1;

            console.log( angle);
            if ( _.some( angle,  v => v >= 5)) {
                return true;
            } else {
                return false;
            }
        },
        isEqual ( x, y, stones) {
            return this.chessboard[x-1][y-1] != stones;
        },
        getCount0( x, y, stones) {
            let count = 1;
            y -= 1;
            if ( y < 1 || this.isEqual( x, y, stones)){
                return count;
            } else {
                return count + this.getCount0( x, y, stones);
            }
        },
        getCount45( x, y, stones) {
            let count = 1;
            x += 1;
            y -= 1;
            if ( x > 15 || y < 1 || this.isEqual( x, y, stones)){
                return count;
            } else {
                return count + this.getCount45( x, y, stones);
            }
        },
        getCount90( x, y, stones) {
            let count = 1;
            x += 1;
            if ( x > 15 || this.isEqual( x, y, stones)){
                return count;
            } else {
                return count + this.getCount90( x, y, stones);
            }
        },
        getCount135( x, y, stones) {
            let count = 1;
            x += 1;
            y += 1;
            if ( x > 15 || y > 15 || this.isEqual( x, y, stones)){
                return count;
            } else {
                return count + this.getCount135( x, y, stones);
            }
        },
        getCount180( x, y, stones) {
            let count = 1;
            y += 1;
            if ( y > 15 || this.isEqual( x, y, stones)){
                return count;
            } else {
                return count + this.getCount180( x, y, stones);
            }
        },
        getCount225( x, y, stones) {
            let count = 1;
            y += 1;
            x -= 1;
            if ( x < 1 || y > 15 || this.isEqual( x, y, stones)){
                return count;
            } else {
                return count + this.getCount225( x, y, stones);
            }
        },
        getCount270( x, y, stones) {
            let count = 1;
            x -= 1;
            if ( x < 1 || this.isEqual( x, y, stones)){
                return count;
            } else {
                return count + this.getCount270( x, y, stones);
            }
        },
        getCount315( x, y, stones) {
            let count = 1;
            x -= 1;
            y -= 1;
            if ( x < 1 || y < 1 || this.isEqual( x, y, stones)){
                return count;
            } else {
                return count + this.getCount315( x, y, stones);
            }
        }
    };

    let chess = new Chess();

    let chessboard = document.createElement('section');
    chessboard.setAttribute( 'id', 'chessboard');
    let chessbox = document.createElement('div');
    chessbox.setAttribute( 'id', 'chessbox');
    let scaleplateX = document.createElement('div');
    scaleplateX.setAttribute( 'id', 'scaleplateX');
    let scaleplateY = document.createElement('div');
    scaleplateY.setAttribute( 'id', 'scaleplateY');
    
    
    for ( let y = 1; y <= 15; y++) {
        let labelY = document.createElement('div');
        labelY.setAttribute( 'class', 'label');
        labelY.innerText = y;
        scaleplateY.appendChild( labelY);

        let labelX = document.createElement('div');
        labelX.setAttribute( 'class', 'label');
        labelX.innerText = alphabet_encode( y);
        scaleplateX.appendChild( labelX);

        for ( let x = 1; x <=15; x++) {
            let grid = document.createElement('div');
            grid.setAttribute( 'class', 'grid');
            grid.setAttribute( 'data-x', alphabet_encode( x));
            grid.setAttribute( 'data-y', y);
            grid.setAttribute( 'data-status', null);

            let piece = document.createElement('div');
            piece.setAttribute( 'class', 'piece');
            let piece_content = document.createElement('div');
            piece_content.setAttribute( 'class', 'piece_content');
            piece.appendChild( piece_content);
            grid.appendChild( piece);
            
            grid.onclick = e => {
                let _this = e.currentTarget;
                let x = _this.getAttribute('data-x'),
                    y = _this.getAttribute('data-y');

                let stones = chess.currentStones();
                let step = chess.addStep( x, y);
                if ( _.isNumber( step)) {
                    _this.setAttribute('data-status', stones);
                    piece_content.innerText = step;
                    setTimeout( () => {
                        if( chess.checkWin()) {
                            let name = stones == 'white'? '白': '黑';
                            alert( name+'方获胜！');
                        }
                    }, 18);
                } else {
                    alert( '怎么可以下别人下过的地方？');
                }
            }

            chessbox.appendChild( grid);
        }
    }
    chessboard.appendChild( scaleplateX);
    chessboard.appendChild( scaleplateY);
    chessboard.appendChild( chessbox);

    document.body.appendChild( chessboard);
})()