/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	var parentJsonpFunction = window["webpackJsonp"];
/******/ 	window["webpackJsonp"] = function webpackJsonpCallback(chunkIds, moreModules, executeModules) {
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [], result;
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(chunkIds, moreModules, executeModules);
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 	};
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// objects to store loaded and loading chunks
/******/ 	var installedChunks = {
/******/ 		2: 0
/******/ 	};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/ 	// This file contains only the entry chunk.
/******/ 	// The chunk loading function for additional chunks
/******/ 	__webpack_require__.e = function requireEnsure(chunkId) {
/******/ 		var installedChunkData = installedChunks[chunkId];
/******/ 		if(installedChunkData === 0) {
/******/ 			return new Promise(function(resolve) { resolve(); });
/******/ 		}
/******/
/******/ 		// a Promise means "currently loading".
/******/ 		if(installedChunkData) {
/******/ 			return installedChunkData[2];
/******/ 		}
/******/
/******/ 		// setup Promise in chunk cache
/******/ 		var promise = new Promise(function(resolve, reject) {
/******/ 			installedChunkData = installedChunks[chunkId] = [resolve, reject];
/******/ 		});
/******/ 		installedChunkData[2] = promise;
/******/
/******/ 		// start chunk loading
/******/ 		var head = document.getElementsByTagName('head')[0];
/******/ 		var script = document.createElement('script');
/******/ 		script.type = 'text/javascript';
/******/ 		script.charset = 'utf-8';
/******/ 		script.async = true;
/******/ 		script.timeout = 120000;
/******/
/******/ 		if (__webpack_require__.nc) {
/******/ 			script.setAttribute("nonce", __webpack_require__.nc);
/******/ 		}
/******/ 		script.src = __webpack_require__.p + "" + ({"0":"lib/lodash","1":"components/aplhabet"}[chunkId]||chunkId) + ".js";
/******/ 		var timeout = setTimeout(onScriptComplete, 120000);
/******/ 		script.onerror = script.onload = onScriptComplete;
/******/ 		function onScriptComplete() {
/******/ 			// avoid mem leaks in IE.
/******/ 			script.onerror = script.onload = null;
/******/ 			clearTimeout(timeout);
/******/ 			var chunk = installedChunks[chunkId];
/******/ 			if(chunk !== 0) {
/******/ 				if(chunk) {
/******/ 					chunk[1](new Error('Loading chunk ' + chunkId + ' failed.'));
/******/ 				}
/******/ 				installedChunks[chunkId] = undefined;
/******/ 			}
/******/ 		};
/******/ 		head.appendChild(script);
/******/
/******/ 		return promise;
/******/ 	};
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// on error function for async loading
/******/ 	__webpack_require__.oe = function(err) { console.error(err); throw err; };
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

( async () => {
    __webpack_require__(1);
    
    let _ = await __webpack_require__(6);
    let { alphabet_encode, alphabet_decode} = await __webpack_require__(7);

    let Chess = function () {
        this.stones = false;
        this.history = [];

        let axle = [];
        for( let i = 0; i < 15; i++ ) {
            let row = [];
            for( let n = 0; n < 15; n++ ) {
                row[n] = null;
            }
            axle[i] = row;
        }
        this.chessboard =  axle;
    };
    
    Chess.prototype = {
        currentStones () {
            return this.stones? 'white': 'black';
        },
        addStep ( x, y) {
            try {
                if( _.some( this.history, { x, y})){
                    return false;
                } else {
                    let stones = this.currentStones();
                    this.chessboard[ alphabet_decode(x) - 1] [y - 1] = stones;
                    this.history.push( { x, y, stones});
                    this.stones = !this.stones;
                    let count = 0;
                    _.map( this.history, v => { 
                        if( v.stones == stones){
                            count++;
                        }
                        return v;
                    })
                    return count;
                }
            } catch (error) {
                console.error( error);
                return false;
            }
        },
        showStep () {
            return this.history;
        },
        checkWin () {
            //  获得最后一次落子
            let { x, y, stones} = _.last( this.history);
            
            console.log( x, y);
            x = Number( alphabet_decode( x));
            y = Number( y);

            let angle = {
                0: this.getCount0( x, y, stones),
                45: this.getCount45( x, y, stones),
                90: this.getCount90( x, y, stones),
                135: this.getCount135( x, y, stones),
                180: this.getCount180( x, y, stones),
                225: this.getCount225( x, y, stones),
                270: this.getCount270( x, y, stones),
                315: this.getCount315( x, y, stones)
            };
            angle._1 = angle[0] + angle[180] - 1;
            angle._2 = angle[45] + angle[225] - 1;
            angle._3 = angle[90] + angle[270] - 1;
            angle._4 = angle[135] + angle[315] - 1;

            console.log( angle);
            if ( _.some( angle,  v => v >= 5)) {
                return true;
            } else {
                return false;
            }
        },
        isEqual ( x, y, stones) {
            return this.chessboard[x-1][y-1] != stones;
        },
        getCount0( x, y, stones) {
            let count = 1;
            y -= 1;
            if ( y < 1 || this.isEqual( x, y, stones)){
                return count;
            } else {
                return count + this.getCount0( x, y, stones);
            }
        },
        getCount45( x, y, stones) {
            let count = 1;
            x += 1;
            y -= 1;
            if ( x > 15 || y < 1 || this.isEqual( x, y, stones)){
                return count;
            } else {
                return count + this.getCount45( x, y, stones);
            }
        },
        getCount90( x, y, stones) {
            let count = 1;
            x += 1;
            if ( x > 15 || this.isEqual( x, y, stones)){
                return count;
            } else {
                return count + this.getCount90( x, y, stones);
            }
        },
        getCount135( x, y, stones) {
            let count = 1;
            x += 1;
            y += 1;
            if ( x > 15 || y > 15 || this.isEqual( x, y, stones)){
                return count;
            } else {
                return count + this.getCount135( x, y, stones);
            }
        },
        getCount180( x, y, stones) {
            let count = 1;
            y += 1;
            if ( y > 15 || this.isEqual( x, y, stones)){
                return count;
            } else {
                return count + this.getCount180( x, y, stones);
            }
        },
        getCount225( x, y, stones) {
            let count = 1;
            y += 1;
            x -= 1;
            if ( x < 1 || y > 15 || this.isEqual( x, y, stones)){
                return count;
            } else {
                return count + this.getCount225( x, y, stones);
            }
        },
        getCount270( x, y, stones) {
            let count = 1;
            x -= 1;
            if ( x < 1 || this.isEqual( x, y, stones)){
                return count;
            } else {
                return count + this.getCount270( x, y, stones);
            }
        },
        getCount315( x, y, stones) {
            let count = 1;
            x -= 1;
            y -= 1;
            if ( x < 1 || y < 1 || this.isEqual( x, y, stones)){
                return count;
            } else {
                return count + this.getCount315( x, y, stones);
            }
        }
    };

    let chess = new Chess();

    let chessboard = document.createElement('section');
    chessboard.setAttribute( 'id', 'chessboard');
    let chessbox = document.createElement('div');
    chessbox.setAttribute( 'id', 'chessbox');
    let scaleplateX = document.createElement('div');
    scaleplateX.setAttribute( 'id', 'scaleplateX');
    let scaleplateY = document.createElement('div');
    scaleplateY.setAttribute( 'id', 'scaleplateY');
    
    
    for ( let y = 1; y <= 15; y++) {
        let labelY = document.createElement('div');
        labelY.setAttribute( 'class', 'label');
        labelY.innerText = y;
        scaleplateY.appendChild( labelY);

        let labelX = document.createElement('div');
        labelX.setAttribute( 'class', 'label');
        labelX.innerText = alphabet_encode( y);
        scaleplateX.appendChild( labelX);

        for ( let x = 1; x <=15; x++) {
            let grid = document.createElement('div');
            grid.setAttribute( 'class', 'grid');
            grid.setAttribute( 'data-x', alphabet_encode( x));
            grid.setAttribute( 'data-y', y);
            grid.setAttribute( 'data-status', null);

            let piece = document.createElement('div');
            piece.setAttribute( 'class', 'piece');
            let piece_content = document.createElement('div');
            piece_content.setAttribute( 'class', 'piece_content');
            piece.appendChild( piece_content);
            grid.appendChild( piece);
            
            grid.onclick = e => {
                let _this = e.currentTarget;
                let x = _this.getAttribute('data-x'),
                    y = _this.getAttribute('data-y');

                let stones = chess.currentStones();
                let step = chess.addStep( x, y);
                if ( _.isNumber( step)) {
                    _this.setAttribute('data-status', stones);
                    piece_content.innerText = step;
                    setTimeout( () => {
                        if( chess.checkWin()) {
                            let name = stones == 'white'? '白': '黑';
                            alert( name+'方获胜！');
                        }
                    }, 18);
                } else {
                    alert( '怎么可以下别人下过的地方？');
                }
            }

            chessbox.appendChild( grid);
        }
    }
    chessboard.appendChild( scaleplateX);
    chessboard.appendChild( scaleplateY);
    chessboard.appendChild( chessbox);

    document.body.appendChild( chessboard);
})()

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(2);
if(typeof content === 'string') content = [[module.i, content, '']];
// Prepare cssTransformation
var transform;

var options = {}
options.transform = transform
// add the styles to the DOM
var update = __webpack_require__(4)(content, options);
if(content.locals) module.exports = content.locals;
// Hot Module Replacement
if(false) {
	// When the styles change, update the <style> tags
	if(!content.locals) {
		module.hot.accept("!!../../node_modules/_css-loader@0.28.7@css-loader/index.js??ref--0-1!../../node_modules/_sass-loader@6.0.6@sass-loader/lib/loader.js??ref--0-2!./index.scss", function() {
			var newContent = require("!!../../node_modules/_css-loader@0.28.7@css-loader/index.js??ref--0-1!../../node_modules/_sass-loader@6.0.6@sass-loader/lib/loader.js??ref--0-2!./index.scss");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
	}
	// When the module is disposed, remove the <style> tags
	module.hot.dispose(function() { update(); });
}

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(3)(true);
// imports


// module
exports.push([module.i, "* {\n  margin: 0;\n  padding: 0; }\n\nhtml,\nbody {\n  width: 100%;\n  height: 100%; }\n\n#chessboard {\n  width: 750px;\n  height: 750px;\n  padding: 15px;\n  position: relative; }\n  #chessboard #scaleplateX,\n  #chessboard #scaleplateY {\n    position: absolute;\n    top: 0;\n    left: 0;\n    box-sizing: border-box; }\n    #chessboard #scaleplateX .label,\n    #chessboard #scaleplateY .label {\n      width: 50px;\n      height: 50px;\n      line-height: 50px;\n      text-align: center;\n      float: left; }\n  #chessboard #scaleplateX {\n    height: 35px;\n    right: 0;\n    padding: 0 15px; }\n    #chessboard #scaleplateX .label {\n      height: 40px;\n      line-height: 40px; }\n  #chessboard #scaleplateY {\n    width: 35px;\n    bottom: 0;\n    padding: 15px 0; }\n    #chessboard #scaleplateY .label {\n      width: 40px;\n      text-align: center; }\n  #chessboard #chessbox:after, #chessboard #chessbox:before {\n    display: table;\n    content: \"\"; }\n  #chessboard #chessbox:after {\n    clear: both; }\n  #chessboard #chessbox .grid {\n    width: 50px;\n    height: 50px;\n    box-sizing: border-box;\n    float: left;\n    cursor: pointer;\n    position: relative; }\n    #chessboard #chessbox .grid:after, #chessboard #chessbox .grid:before {\n      position: absolute;\n      background: #000;\n      content: \"\"; }\n    #chessboard #chessbox .grid:after {\n      width: 2px;\n      height: 100%;\n      left: 50%;\n      margin-left: -1px; }\n    #chessboard #chessbox .grid:before {\n      height: 2px;\n      width: 100%;\n      top: 50%;\n      margin-top: -1px; }\n    #chessboard #chessbox .grid[data-y=\"1\"]:after {\n      top: 50%; }\n    #chessboard #chessbox .grid[data-y=\"15\"]:after {\n      bottom: 50%; }\n    #chessboard #chessbox .grid[data-x=\"A\"]:before {\n      left: 50%; }\n    #chessboard #chessbox .grid[data-x=\"O\"]:before {\n      right: 50%; }\n    #chessboard #chessbox .grid .piece {\n      position: absolute;\n      z-index: 2;\n      top: 0;\n      left: 0;\n      right: 0;\n      bottom: 0;\n      padding: 3px;\n      box-sizing: border-box; }\n      #chessboard #chessbox .grid .piece .piece_content {\n        position: absolute;\n        z-index: 2;\n        top: 0;\n        left: 0;\n        right: 0;\n        bottom: 0;\n        text-align: center;\n        font-size: 14px;\n        line-height: 50px; }\n      #chessboard #chessbox .grid .piece:after {\n        content: '';\n        top: 50%;\n        left: 50%;\n        position: absolute;\n        border-radius: 50%;\n        box-sizing: border-box; }\n    #chessboard #chessbox .grid[data-x=\"D\"][data-y=\"4\"] .piece:after, #chessboard #chessbox .grid[data-x=\"D\"][data-y=\"12\"] .piece:after, #chessboard #chessbox .grid[data-x=\"L\"][data-y=\"4\"] .piece:after, #chessboard #chessbox .grid[data-x=\"L\"][data-y=\"12\"] .piece:after, #chessboard #chessbox .grid[data-x=\"H\"][data-y=\"8\"] .piece:after {\n      margin-top: -7px;\n      margin-left: -7px;\n      width: 14px;\n      height: 14px;\n      background: #000; }\n    #chessboard #chessbox .grid[data-status=\"white\"] .piece .piece_content {\n      color: #000; }\n    #chessboard #chessbox .grid[data-status=\"white\"] .piece:after {\n      margin-top: -23px !important;\n      margin-left: -23px !important;\n      width: 46px !important;\n      height: 46px !important;\n      background: #fff !important;\n      border: 1px solid #000 !important; }\n    #chessboard #chessbox .grid[data-status=\"black\"] .piece .piece_content {\n      color: #fff; }\n    #chessboard #chessbox .grid[data-status=\"black\"] .piece:after {\n      margin-top: -23px !important;\n      margin-left: -23px !important;\n      width: 46px !important;\n      height: 46px !important;\n      background: #000 !important;\n      border: 1px solid #000 !important; }\n", "", {"version":3,"sources":["G:/www/gobang/src/scss/src/scss/index.scss"],"names":[],"mappings":"AAAA;EACI,UAAS;EACT,WAAU,EACb;;AACD;;EAEI,YAAW;EACX,aAAY,EACf;;AAED;EACI,aAAmB;EACnB,cAAoB;EACpB,cAAa;EACb,mBAAkB,EAoMrB;EAxMD;;IAQQ,mBAAkB;IAClB,OAAM;IACN,QAAO;IACP,uBAAsB,EAQzB;IAnBL;;MAaY,YAAW;MACX,aAAY;MACZ,kBAAiB;MACjB,mBAAkB;MAClB,YAAW,EACd;EAlBT;IAsBQ,aAAY;IACZ,SAAQ;IACR,gBAAe,EAKlB;IA7BL;MA0BY,aAAY;MACZ,kBAAiB,EACpB;EA5BT;IAgCQ,YAAW;IACX,UAAS;IACT,gBAAe,EAKlB;IAvCL;MAoCY,YAAW;MACX,mBAAkB,EACrB;EAtCT;IA6CY,eAAc;IACd,YAAW,EACd;EA/CT;IAkDY,YAAW,EACd;EAnDT;IAsDY,YAAW;IACX,aAAY;IACZ,uBAAsB;IAEtB,YAAW;IACX,gBAAe;IACf,mBAAkB,EA0IrB;IAtMT;MAgEgB,mBAAkB;MAClB,iBAAgB;MAChB,YAAW,EACd;IAnEb;MAsEgB,WAAU;MACV,aAAY;MACZ,UAAS;MACT,kBAAiB,EACpB;IA1Eb;MA6EgB,YAAW;MACX,YAAW;MACX,SAAQ;MACR,iBAAgB,EACnB;IAjFb;MAqFoB,SAAQ,EACX;IAtFjB;MA2FoB,YAAW,EACd;IA5FjB;MAiGoB,UAAS,EACZ;IAlGjB;MAuGoB,WAAU,EACb;IAxGjB;MA4GgB,mBAAkB;MAClB,WAAU;MACV,OAAM;MACN,QAAO;MACP,SAAQ;MACR,UAAS;MACT,aAAY;MACZ,uBAAsB,EAsBzB;MAzIb;QAsHoB,mBAAkB;QAClB,WAAU;QACV,OAAM;QACN,QAAO;QACP,SAAQ;QACR,UAAS;QACT,mBAAkB;QAClB,gBAAe;QACf,kBAAiB,EACpB;MA/HjB;QAkIoB,YAAW;QACX,SAAQ;QACR,UAAS;QACT,mBAAkB;QAClB,mBAAkB;QAClB,uBAAsB,EACzB;IAxIjB;MA6IoB,iBAAgB;MAChB,kBAAiB;MACjB,YAAW;MACX,aAAY;MACZ,iBAAgB,EACnB;IAlJjB;MAwLwB,YAAW,EACd;IAzLrB;MAuJoB,6BAA2B;MAC3B,8BAA4B;MAC5B,uBAAqB;MACrB,wBAAsB;MACtB,4BAA0B;MAC1B,kCAAgC,EACnC;IA7JjB;MAiMwB,YAAW,EACd;IAlMrB;MAkKoB,6BAA2B;MAC3B,8BAA4B;MAC5B,uBAAqB;MACrB,wBAAsB;MACtB,4BAA0B;MAC1B,kCAAgC,EACnC","file":"index.scss","sourcesContent":["* {\r\n    margin: 0;\r\n    padding: 0;\r\n}\r\nhtml,\r\nbody {\r\n    width: 100%;\r\n    height: 100%;\r\n}\r\n\r\n#chessboard {\r\n    width: 15 * 50 + px;\r\n    height: 15 * 50 + px;\r\n    padding: 15px;\r\n    position: relative;\r\n\r\n    #scaleplateX,\r\n    #scaleplateY {\r\n        position: absolute;\r\n        top: 0;\r\n        left: 0;\r\n        box-sizing: border-box;\r\n        .label {\r\n            width: 50px;\r\n            height: 50px;\r\n            line-height: 50px;\r\n            text-align: center;\r\n            float: left;\r\n        }\r\n    }\r\n\r\n    #scaleplateX {\r\n        height: 35px;\r\n        right: 0;\r\n        padding: 0 15px;\r\n        .label {\r\n            height: 40px;\r\n            line-height: 40px;\r\n        }\r\n    }\r\n\r\n    #scaleplateY {\r\n        width: 35px;\r\n        bottom: 0;\r\n        padding: 15px 0;\r\n        .label {\r\n            width: 40px;\r\n            text-align: center;\r\n        }\r\n    }\r\n\r\n    #chessbox {\r\n        \r\n        &:after,\r\n        &:before {\r\n            display: table;\r\n            content: \"\";\r\n        }\r\n\r\n        &:after {\r\n            clear: both;\r\n        }\r\n\r\n        .grid {\r\n            width: 50px;\r\n            height: 50px;\r\n            box-sizing: border-box;\r\n            \r\n            float: left;\r\n            cursor: pointer;\r\n            position: relative;\r\n    \r\n            &:after,\r\n            &:before {\r\n                position: absolute;\r\n                background: #000;\r\n                content: \"\";\r\n            }\r\n    \r\n            &:after {\r\n                width: 2px;\r\n                height: 100%;\r\n                left: 50%;\r\n                margin-left: -1px;\r\n            }\r\n    \r\n            &:before {\r\n                height: 2px;\r\n                width: 100%;\r\n                top: 50%;\r\n                margin-top: -1px;\r\n            }\r\n\r\n            &[data-y=\"1\"] {\r\n                &:after {\r\n                    top: 50%;\r\n                }\r\n            }\r\n            \r\n            &[data-y=\"15\"] {\r\n                &:after {\r\n                    bottom: 50%;\r\n                }\r\n            }\r\n\r\n            &[data-x=\"A\"] {\r\n                &:before {\r\n                    left: 50%;\r\n                }\r\n            }\r\n\r\n            &[data-x=\"O\"] {\r\n                &:before {\r\n                    right: 50%;\r\n                }\r\n            }\r\n\r\n            .piece {\r\n                position: absolute;\r\n                z-index: 2;\r\n                top: 0;\r\n                left: 0;\r\n                right: 0;\r\n                bottom: 0;\r\n                padding: 3px;\r\n                box-sizing: border-box;\r\n\r\n                .piece_content{\r\n                    position: absolute;\r\n                    z-index: 2;\r\n                    top: 0;\r\n                    left: 0;\r\n                    right: 0;\r\n                    bottom: 0;\r\n                    text-align: center;\r\n                    font-size: 14px;\r\n                    line-height: 50px;\r\n                }\r\n                \r\n                &:after {\r\n                    content: '';\r\n                    top: 50%;\r\n                    left: 50%;\r\n                    position: absolute;\r\n                    border-radius: 50%;\r\n                    box-sizing: border-box;\r\n                }\r\n            }\r\n\r\n            @mixin star () {\r\n                &:after {\r\n                    margin-top: -7px;\r\n                    margin-left: -7px;\r\n                    width: 14px;\r\n                    height: 14px;\r\n                    background: #000;\r\n                }\r\n            }\r\n\r\n            @mixin white-stones () {\r\n                &:after {\r\n                    margin-top: -23px!important;\r\n                    margin-left: -23px!important;\r\n                    width: 46px!important;\r\n                    height: 46px!important;\r\n                    background: #fff!important;\r\n                    border: 1px solid #000!important;\r\n                }\r\n            }\r\n\r\n            @mixin black-stones () {\r\n                &:after {\r\n                    margin-top: -23px!important;\r\n                    margin-left: -23px!important;\r\n                    width: 46px!important;\r\n                    height: 46px!important;\r\n                    background: #000!important;\r\n                    border: 1px solid #000!important;\r\n                }\r\n            }\r\n\r\n            &[data-x=\"D\"][data-y=\"4\"],\r\n            &[data-x=\"D\"][data-y=\"12\"],\r\n            &[data-x=\"L\"][data-y=\"4\"],\r\n            &[data-x=\"L\"][data-y=\"12\"],\r\n            &[data-x=\"H\"][data-y=\"8\"] {\r\n                .piece {\r\n                    @include star();\r\n                }\r\n            }\r\n\r\n            &[data-status=\"white\"]{\r\n                .piece {\r\n                    .piece_content{\r\n                        color: #000;\r\n                    }\r\n                    @include white-stones();\r\n                }\r\n            }\r\n\r\n            &[data-status=\"black\"]{\r\n                .piece {\r\n                    .piece_content{\r\n                        color: #fff;\r\n                    }\r\n                    @include black-stones();\r\n                }\r\n            }\r\n        }\r\n    }\r\n}"],"sourceRoot":""}]);

// exports


/***/ }),
/* 3 */
/***/ (function(module, exports) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader
module.exports = function(useSourceMap) {
	var list = [];

	// return the list of modules as css string
	list.toString = function toString() {
		return this.map(function (item) {
			var content = cssWithMappingToString(item, useSourceMap);
			if(item[2]) {
				return "@media " + item[2] + "{" + content + "}";
			} else {
				return content;
			}
		}).join("");
	};

	// import a list of modules into the list
	list.i = function(modules, mediaQuery) {
		if(typeof modules === "string")
			modules = [[null, modules, ""]];
		var alreadyImportedModules = {};
		for(var i = 0; i < this.length; i++) {
			var id = this[i][0];
			if(typeof id === "number")
				alreadyImportedModules[id] = true;
		}
		for(i = 0; i < modules.length; i++) {
			var item = modules[i];
			// skip already imported module
			// this implementation is not 100% perfect for weird media query combinations
			//  when a module is imported multiple times with different media queries.
			//  I hope this will never occur (Hey this way we have smaller bundles)
			if(typeof item[0] !== "number" || !alreadyImportedModules[item[0]]) {
				if(mediaQuery && !item[2]) {
					item[2] = mediaQuery;
				} else if(mediaQuery) {
					item[2] = "(" + item[2] + ") and (" + mediaQuery + ")";
				}
				list.push(item);
			}
		}
	};
	return list;
};

function cssWithMappingToString(item, useSourceMap) {
	var content = item[1] || '';
	var cssMapping = item[3];
	if (!cssMapping) {
		return content;
	}

	if (useSourceMap && typeof btoa === 'function') {
		var sourceMapping = toComment(cssMapping);
		var sourceURLs = cssMapping.sources.map(function (source) {
			return '/*# sourceURL=' + cssMapping.sourceRoot + source + ' */'
		});

		return [content].concat(sourceURLs).concat([sourceMapping]).join('\n');
	}

	return [content].join('\n');
}

// Adapted from convert-source-map (MIT)
function toComment(sourceMap) {
	// eslint-disable-next-line no-undef
	var base64 = btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap))));
	var data = 'sourceMappingURL=data:application/json;charset=utf-8;base64,' + base64;

	return '/*# ' + data + ' */';
}


/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/

var stylesInDom = {};

var	memoize = function (fn) {
	var memo;

	return function () {
		if (typeof memo === "undefined") memo = fn.apply(this, arguments);
		return memo;
	};
};

var isOldIE = memoize(function () {
	// Test for IE <= 9 as proposed by Browserhacks
	// @see http://browserhacks.com/#hack-e71d8692f65334173fee715c222cb805
	// Tests for existence of standard globals is to allow style-loader
	// to operate correctly into non-standard environments
	// @see https://github.com/webpack-contrib/style-loader/issues/177
	return window && document && document.all && !window.atob;
});

var getElement = (function (fn) {
	var memo = {};

	return function(selector) {
		if (typeof memo[selector] === "undefined") {
			memo[selector] = fn.call(this, selector);
		}

		return memo[selector]
	};
})(function (target) {
	return document.querySelector(target)
});

var singleton = null;
var	singletonCounter = 0;
var	stylesInsertedAtTop = [];

var	fixUrls = __webpack_require__(5);

module.exports = function(list, options) {
	if (typeof DEBUG !== "undefined" && DEBUG) {
		if (typeof document !== "object") throw new Error("The style-loader cannot be used in a non-browser environment");
	}

	options = options || {};

	options.attrs = typeof options.attrs === "object" ? options.attrs : {};

	// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
	// tags it will allow on a page
	if (!options.singleton) options.singleton = isOldIE();

	// By default, add <style> tags to the <head> element
	if (!options.insertInto) options.insertInto = "head";

	// By default, add <style> tags to the bottom of the target
	if (!options.insertAt) options.insertAt = "bottom";

	var styles = listToStyles(list, options);

	addStylesToDom(styles, options);

	return function update (newList) {
		var mayRemove = [];

		for (var i = 0; i < styles.length; i++) {
			var item = styles[i];
			var domStyle = stylesInDom[item.id];

			domStyle.refs--;
			mayRemove.push(domStyle);
		}

		if(newList) {
			var newStyles = listToStyles(newList, options);
			addStylesToDom(newStyles, options);
		}

		for (var i = 0; i < mayRemove.length; i++) {
			var domStyle = mayRemove[i];

			if(domStyle.refs === 0) {
				for (var j = 0; j < domStyle.parts.length; j++) domStyle.parts[j]();

				delete stylesInDom[domStyle.id];
			}
		}
	};
};

function addStylesToDom (styles, options) {
	for (var i = 0; i < styles.length; i++) {
		var item = styles[i];
		var domStyle = stylesInDom[item.id];

		if(domStyle) {
			domStyle.refs++;

			for(var j = 0; j < domStyle.parts.length; j++) {
				domStyle.parts[j](item.parts[j]);
			}

			for(; j < item.parts.length; j++) {
				domStyle.parts.push(addStyle(item.parts[j], options));
			}
		} else {
			var parts = [];

			for(var j = 0; j < item.parts.length; j++) {
				parts.push(addStyle(item.parts[j], options));
			}

			stylesInDom[item.id] = {id: item.id, refs: 1, parts: parts};
		}
	}
}

function listToStyles (list, options) {
	var styles = [];
	var newStyles = {};

	for (var i = 0; i < list.length; i++) {
		var item = list[i];
		var id = options.base ? item[0] + options.base : item[0];
		var css = item[1];
		var media = item[2];
		var sourceMap = item[3];
		var part = {css: css, media: media, sourceMap: sourceMap};

		if(!newStyles[id]) styles.push(newStyles[id] = {id: id, parts: [part]});
		else newStyles[id].parts.push(part);
	}

	return styles;
}

function insertStyleElement (options, style) {
	var target = getElement(options.insertInto)

	if (!target) {
		throw new Error("Couldn't find a style target. This probably means that the value for the 'insertInto' parameter is invalid.");
	}

	var lastStyleElementInsertedAtTop = stylesInsertedAtTop[stylesInsertedAtTop.length - 1];

	if (options.insertAt === "top") {
		if (!lastStyleElementInsertedAtTop) {
			target.insertBefore(style, target.firstChild);
		} else if (lastStyleElementInsertedAtTop.nextSibling) {
			target.insertBefore(style, lastStyleElementInsertedAtTop.nextSibling);
		} else {
			target.appendChild(style);
		}
		stylesInsertedAtTop.push(style);
	} else if (options.insertAt === "bottom") {
		target.appendChild(style);
	} else {
		throw new Error("Invalid value for parameter 'insertAt'. Must be 'top' or 'bottom'.");
	}
}

function removeStyleElement (style) {
	if (style.parentNode === null) return false;
	style.parentNode.removeChild(style);

	var idx = stylesInsertedAtTop.indexOf(style);
	if(idx >= 0) {
		stylesInsertedAtTop.splice(idx, 1);
	}
}

function createStyleElement (options) {
	var style = document.createElement("style");

	options.attrs.type = "text/css";

	addAttrs(style, options.attrs);
	insertStyleElement(options, style);

	return style;
}

function createLinkElement (options) {
	var link = document.createElement("link");

	options.attrs.type = "text/css";
	options.attrs.rel = "stylesheet";

	addAttrs(link, options.attrs);
	insertStyleElement(options, link);

	return link;
}

function addAttrs (el, attrs) {
	Object.keys(attrs).forEach(function (key) {
		el.setAttribute(key, attrs[key]);
	});
}

function addStyle (obj, options) {
	var style, update, remove, result;

	// If a transform function was defined, run it on the css
	if (options.transform && obj.css) {
	    result = options.transform(obj.css);

	    if (result) {
	    	// If transform returns a value, use that instead of the original css.
	    	// This allows running runtime transformations on the css.
	    	obj.css = result;
	    } else {
	    	// If the transform function returns a falsy value, don't add this css.
	    	// This allows conditional loading of css
	    	return function() {
	    		// noop
	    	};
	    }
	}

	if (options.singleton) {
		var styleIndex = singletonCounter++;

		style = singleton || (singleton = createStyleElement(options));

		update = applyToSingletonTag.bind(null, style, styleIndex, false);
		remove = applyToSingletonTag.bind(null, style, styleIndex, true);

	} else if (
		obj.sourceMap &&
		typeof URL === "function" &&
		typeof URL.createObjectURL === "function" &&
		typeof URL.revokeObjectURL === "function" &&
		typeof Blob === "function" &&
		typeof btoa === "function"
	) {
		style = createLinkElement(options);
		update = updateLink.bind(null, style, options);
		remove = function () {
			removeStyleElement(style);

			if(style.href) URL.revokeObjectURL(style.href);
		};
	} else {
		style = createStyleElement(options);
		update = applyToTag.bind(null, style);
		remove = function () {
			removeStyleElement(style);
		};
	}

	update(obj);

	return function updateStyle (newObj) {
		if (newObj) {
			if (
				newObj.css === obj.css &&
				newObj.media === obj.media &&
				newObj.sourceMap === obj.sourceMap
			) {
				return;
			}

			update(obj = newObj);
		} else {
			remove();
		}
	};
}

var replaceText = (function () {
	var textStore = [];

	return function (index, replacement) {
		textStore[index] = replacement;

		return textStore.filter(Boolean).join('\n');
	};
})();

function applyToSingletonTag (style, index, remove, obj) {
	var css = remove ? "" : obj.css;

	if (style.styleSheet) {
		style.styleSheet.cssText = replaceText(index, css);
	} else {
		var cssNode = document.createTextNode(css);
		var childNodes = style.childNodes;

		if (childNodes[index]) style.removeChild(childNodes[index]);

		if (childNodes.length) {
			style.insertBefore(cssNode, childNodes[index]);
		} else {
			style.appendChild(cssNode);
		}
	}
}

function applyToTag (style, obj) {
	var css = obj.css;
	var media = obj.media;

	if(media) {
		style.setAttribute("media", media)
	}

	if(style.styleSheet) {
		style.styleSheet.cssText = css;
	} else {
		while(style.firstChild) {
			style.removeChild(style.firstChild);
		}

		style.appendChild(document.createTextNode(css));
	}
}

function updateLink (link, options, obj) {
	var css = obj.css;
	var sourceMap = obj.sourceMap;

	/*
		If convertToAbsoluteUrls isn't defined, but sourcemaps are enabled
		and there is no publicPath defined then lets turn convertToAbsoluteUrls
		on by default.  Otherwise default to the convertToAbsoluteUrls option
		directly
	*/
	var autoFixUrls = options.convertToAbsoluteUrls === undefined && sourceMap;

	if (options.convertToAbsoluteUrls || autoFixUrls) {
		css = fixUrls(css);
	}

	if (sourceMap) {
		// http://stackoverflow.com/a/26603875
		css += "\n/*# sourceMappingURL=data:application/json;base64," + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + " */";
	}

	var blob = new Blob([css], { type: "text/css" });

	var oldSrc = link.href;

	link.href = URL.createObjectURL(blob);

	if(oldSrc) URL.revokeObjectURL(oldSrc);
}


/***/ }),
/* 5 */
/***/ (function(module, exports) {


/**
 * When source maps are enabled, `style-loader` uses a link element with a data-uri to
 * embed the css on the page. This breaks all relative urls because now they are relative to a
 * bundle instead of the current page.
 *
 * One solution is to only use full urls, but that may be impossible.
 *
 * Instead, this function "fixes" the relative urls to be absolute according to the current page location.
 *
 * A rudimentary test suite is located at `test/fixUrls.js` and can be run via the `npm test` command.
 *
 */

module.exports = function (css) {
  // get current location
  var location = typeof window !== "undefined" && window.location;

  if (!location) {
    throw new Error("fixUrls requires window.location");
  }

	// blank or null?
	if (!css || typeof css !== "string") {
	  return css;
  }

  var baseUrl = location.protocol + "//" + location.host;
  var currentDir = baseUrl + location.pathname.replace(/\/[^\/]*$/, "/");

	// convert each url(...)
	/*
	This regular expression is just a way to recursively match brackets within
	a string.

	 /url\s*\(  = Match on the word "url" with any whitespace after it and then a parens
	   (  = Start a capturing group
	     (?:  = Start a non-capturing group
	         [^)(]  = Match anything that isn't a parentheses
	         |  = OR
	         \(  = Match a start parentheses
	             (?:  = Start another non-capturing groups
	                 [^)(]+  = Match anything that isn't a parentheses
	                 |  = OR
	                 \(  = Match a start parentheses
	                     [^)(]*  = Match anything that isn't a parentheses
	                 \)  = Match a end parentheses
	             )  = End Group
              *\) = Match anything and then a close parens
          )  = Close non-capturing group
          *  = Match anything
       )  = Close capturing group
	 \)  = Match a close parens

	 /gi  = Get all matches, not the first.  Be case insensitive.
	 */
	var fixedCss = css.replace(/url\s*\(((?:[^)(]|\((?:[^)(]+|\([^)(]*\))*\))*)\)/gi, function(fullMatch, origUrl) {
		// strip quotes (if they exist)
		var unquotedOrigUrl = origUrl
			.trim()
			.replace(/^"(.*)"$/, function(o, $1){ return $1; })
			.replace(/^'(.*)'$/, function(o, $1){ return $1; });

		// already a full url? no change
		if (/^(#|data:|http:\/\/|https:\/\/|file:\/\/\/)/i.test(unquotedOrigUrl)) {
		  return fullMatch;
		}

		// convert the url to a full url
		var newUrl;

		if (unquotedOrigUrl.indexOf("//") === 0) {
		  	//TODO: should we add protocol?
			newUrl = unquotedOrigUrl;
		} else if (unquotedOrigUrl.indexOf("/") === 0) {
			// path should be relative to the base url
			newUrl = baseUrl + unquotedOrigUrl; // already starts with '/'
		} else {
			// path should be relative to current directory
			newUrl = currentDir + unquotedOrigUrl.replace(/^\.\//, ""); // Strip leading './'
		}

		// send back the fixed url(...)
		return "url(" + JSON.stringify(newUrl) + ")";
	});

	// send back the fixed css
	return fixedCss;
};


/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.e/* require.ensure */(0).then((require => __webpack_require__(8)).bind(null, __webpack_require__)).catch(__webpack_require__.oe)

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.e/* require.ensure */(1).then((require => __webpack_require__(9)).bind(null, __webpack_require__)).catch(__webpack_require__.oe)

/***/ })
/******/ ]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgZTc2Mzg3OTcyOGJiOGNjYWE5ZTYiLCJ3ZWJwYWNrOi8vLy4vc3JjL21haW4uanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3Njc3MvaW5kZXguc2Nzcz84NTI1Iiwid2VicGFjazovLy8uL3NyYy9zY3NzL2luZGV4LnNjc3MiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL19jc3MtbG9hZGVyQDAuMjguN0Bjc3MtbG9hZGVyL2xpYi9jc3MtYmFzZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvX3N0eWxlLWxvYWRlckAwLjE4LjJAc3R5bGUtbG9hZGVyL2xpYi9hZGRTdHlsZXMuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL19zdHlsZS1sb2FkZXJAMC4xOC4yQHN0eWxlLWxvYWRlci9saWIvdXJscy5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvbGliL2xvZGFzaC5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvY29tcG9uZW50cy9hbHBoYWJldC9pbmRleC5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZ0JBQVEsb0JBQW9CO0FBQzVCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esa0RBQTBDLFdBQVcsRUFBRTtBQUN2RDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxZQUFJO0FBQ0o7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsdURBQStDLDJDQUEyQztBQUMxRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBMkIsMEJBQTBCLEVBQUU7QUFDdkQseUNBQWlDLGVBQWU7QUFDaEQ7QUFDQTtBQUNBOztBQUVBO0FBQ0EsOERBQXNELCtEQUErRDs7QUFFckg7QUFDQTs7QUFFQTtBQUNBLGtEQUEwQyxvQkFBb0IsV0FBVzs7QUFFekU7QUFDQTs7Ozs7OztBQy9JQTtBQUNBOztBQUVBO0FBQ0EsU0FBUyxrQ0FBa0M7O0FBRTNDO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLHVCQUF1QixRQUFRO0FBQy9CO0FBQ0EsMkJBQTJCLFFBQVE7QUFDbkM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0EsMkNBQTJDLE1BQU07QUFDakQ7QUFDQSxpQkFBaUI7QUFDakI7QUFDQTtBQUNBLHdDQUF3QyxjQUFjO0FBQ3REO0FBQ0E7QUFDQSwrQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscUJBQXFCO0FBQ3JCO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBLGlCQUFpQixjQUFjOztBQUUvQjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0Esb0JBQW9CLFNBQVM7QUFDN0I7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsd0JBQXdCLFFBQVE7QUFDaEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxQkFBcUI7QUFDckIsaUJBQWlCO0FBQ2pCO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxDQUFDLEc7Ozs7OztBQ3BPRDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQSxnQ0FBZ0MsVUFBVSxFQUFFO0FBQzVDLEM7Ozs7OztBQ3pCQTtBQUNBOzs7QUFHQTtBQUNBLDRCQUE2QixjQUFjLGVBQWUsRUFBRSxpQkFBaUIsZ0JBQWdCLGlCQUFpQixFQUFFLGlCQUFpQixpQkFBaUIsa0JBQWtCLGtCQUFrQix1QkFBdUIsRUFBRSwyREFBMkQseUJBQXlCLGFBQWEsY0FBYyw2QkFBNkIsRUFBRSw2RUFBNkUsb0JBQW9CLHFCQUFxQiwwQkFBMEIsMkJBQTJCLG9CQUFvQixFQUFFLDhCQUE4QixtQkFBbUIsZUFBZSxzQkFBc0IsRUFBRSx1Q0FBdUMscUJBQXFCLDBCQUEwQixFQUFFLDhCQUE4QixrQkFBa0IsZ0JBQWdCLHNCQUFzQixFQUFFLHVDQUF1QyxvQkFBb0IsMkJBQTJCLEVBQUUsK0RBQStELHFCQUFxQixvQkFBb0IsRUFBRSxpQ0FBaUMsa0JBQWtCLEVBQUUsaUNBQWlDLGtCQUFrQixtQkFBbUIsNkJBQTZCLGtCQUFrQixzQkFBc0IseUJBQXlCLEVBQUUsNkVBQTZFLDJCQUEyQix5QkFBeUIsc0JBQXNCLEVBQUUseUNBQXlDLG1CQUFtQixxQkFBcUIsa0JBQWtCLDBCQUEwQixFQUFFLDBDQUEwQyxvQkFBb0Isb0JBQW9CLGlCQUFpQix5QkFBeUIsRUFBRSx1REFBdUQsaUJBQWlCLEVBQUUsd0RBQXdELG9CQUFvQixFQUFFLHdEQUF3RCxrQkFBa0IsRUFBRSx3REFBd0QsbUJBQW1CLEVBQUUsMENBQTBDLDJCQUEyQixtQkFBbUIsZUFBZSxnQkFBZ0IsaUJBQWlCLGtCQUFrQixxQkFBcUIsK0JBQStCLEVBQUUsMkRBQTJELDZCQUE2QixxQkFBcUIsaUJBQWlCLGtCQUFrQixtQkFBbUIsb0JBQW9CLDZCQUE2QiwwQkFBMEIsNEJBQTRCLEVBQUUsa0RBQWtELHNCQUFzQixtQkFBbUIsb0JBQW9CLDZCQUE2Qiw2QkFBNkIsaUNBQWlDLEVBQUUsc1dBQXNXLHlCQUF5QiwwQkFBMEIsb0JBQW9CLHFCQUFxQix5QkFBeUIsRUFBRSxnRkFBZ0Ysb0JBQW9CLEVBQUUsdUVBQXVFLHFDQUFxQyxzQ0FBc0MsK0JBQStCLGdDQUFnQyxvQ0FBb0MsMENBQTBDLEVBQUUsZ0ZBQWdGLG9CQUFvQixFQUFFLHVFQUF1RSxxQ0FBcUMsc0NBQXNDLCtCQUErQixnQ0FBZ0Msb0NBQW9DLDBDQUEwQyxFQUFFLFVBQVUsaUdBQWlHLFVBQVUsZ0JBQWdCLE1BQU0sVUFBVSxnQkFBZ0IsS0FBSyxXQUFXLFlBQVksV0FBVyxtQkFBbUIsT0FBTyxZQUFZLFdBQVcsVUFBVSxrQkFBa0IsT0FBTyxVQUFVLFVBQVUsWUFBWSxhQUFhLGdCQUFnQixNQUFNLFdBQVcsVUFBVSxpQkFBaUIsTUFBTSxXQUFXLGtCQUFrQixNQUFNLFdBQVcsVUFBVSxpQkFBaUIsTUFBTSxXQUFXLGtCQUFrQixNQUFNLFdBQVcsZUFBZSxNQUFNLGdCQUFnQixNQUFNLFdBQVcsVUFBVSxZQUFZLFdBQVcsV0FBVyxtQkFBbUIsTUFBTSxjQUFjLGFBQWEsZ0JBQWdCLE1BQU0sWUFBWSxVQUFVLFVBQVUsa0JBQWtCLE1BQU0sWUFBWSxVQUFVLFVBQVUsa0JBQWtCLE1BQU0saUJBQWlCLE9BQU8saUJBQWlCLE9BQU8saUJBQWlCLE9BQU8saUJBQWlCLE9BQU8sY0FBYyxXQUFXLFVBQVUsVUFBVSxVQUFVLFVBQVUsVUFBVSxtQkFBbUIsTUFBTSxjQUFjLFdBQVcsVUFBVSxVQUFVLFVBQVUsVUFBVSxZQUFZLFlBQVksa0JBQWtCLE9BQU8sWUFBWSxVQUFVLFVBQVUsWUFBWSxhQUFhLG1CQUFtQixPQUFPLGNBQWMsYUFBYSxXQUFXLFVBQVUsa0JBQWtCLE9BQU8saUJBQWlCLE9BQU8sY0FBYyxhQUFhLGFBQWEsYUFBYSxhQUFhLG1CQUFtQixPQUFPLGlCQUFpQixPQUFPLGNBQWMsYUFBYSxhQUFhLGFBQWEsYUFBYSw4REFBOEQsa0JBQWtCLG1CQUFtQixLQUFLLG1CQUFtQixvQkFBb0IscUJBQXFCLEtBQUsscUJBQXFCLDRCQUE0Qiw2QkFBNkIsc0JBQXNCLDJCQUEyQiwrQ0FBK0MsK0JBQStCLG1CQUFtQixvQkFBb0IsbUNBQW1DLG9CQUFvQiw0QkFBNEIsNkJBQTZCLGtDQUFrQyxtQ0FBbUMsNEJBQTRCLGFBQWEsU0FBUywwQkFBMEIseUJBQXlCLHFCQUFxQiw0QkFBNEIsb0JBQW9CLDZCQUE2QixrQ0FBa0MsYUFBYSxTQUFTLDBCQUEwQix3QkFBd0Isc0JBQXNCLDRCQUE0QixvQkFBb0IsNEJBQTRCLG1DQUFtQyxhQUFhLFNBQVMsdUJBQXVCLHNEQUFzRCwrQkFBK0IsOEJBQThCLGFBQWEseUJBQXlCLDRCQUE0QixhQUFhLHVCQUF1Qiw0QkFBNEIsNkJBQTZCLHVDQUF1Qyw0Q0FBNEMsZ0NBQWdDLG1DQUFtQywwREFBMEQsdUNBQXVDLHFDQUFxQyxrQ0FBa0MsaUJBQWlCLGlDQUFpQywrQkFBK0IsaUNBQWlDLDhCQUE4QixzQ0FBc0MsaUJBQWlCLGtDQUFrQyxnQ0FBZ0MsZ0NBQWdDLDZCQUE2QixxQ0FBcUMsaUJBQWlCLHFDQUFxQyw2QkFBNkIsaUNBQWlDLHFCQUFxQixpQkFBaUIsa0RBQWtELDZCQUE2QixvQ0FBb0MscUJBQXFCLGlCQUFpQixxQ0FBcUMsOEJBQThCLGtDQUFrQyxxQkFBcUIsaUJBQWlCLHFDQUFxQyw4QkFBOEIsbUNBQW1DLHFCQUFxQixpQkFBaUIsNEJBQTRCLHVDQUF1QywrQkFBK0IsMkJBQTJCLDRCQUE0Qiw2QkFBNkIsOEJBQThCLGlDQUFpQywyQ0FBMkMsdUNBQXVDLDJDQUEyQyxtQ0FBbUMsK0JBQStCLGdDQUFnQyxpQ0FBaUMsa0NBQWtDLDJDQUEyQyx3Q0FBd0MsMENBQTBDLHFCQUFxQixpREFBaUQsb0NBQW9DLGlDQUFpQyxrQ0FBa0MsMkNBQTJDLDJDQUEyQywrQ0FBK0MscUJBQXFCLGlCQUFpQixvQ0FBb0MsNkJBQTZCLHlDQUF5QywwQ0FBMEMsb0NBQW9DLHFDQUFxQyx5Q0FBeUMscUJBQXFCLGlCQUFpQiw0Q0FBNEMsNkJBQTZCLG9EQUFvRCxxREFBcUQsOENBQThDLCtDQUErQyxtREFBbUQseURBQXlELHFCQUFxQixpQkFBaUIsNENBQTRDLDZCQUE2QixvREFBb0QscURBQXFELDhDQUE4QywrQ0FBK0MsbURBQW1ELHlEQUF5RCxxQkFBcUIsaUJBQWlCLDZPQUE2Tyw0QkFBNEIsd0NBQXdDLHFCQUFxQixpQkFBaUIsNkNBQTZDLDRCQUE0Qix1Q0FBdUMsd0NBQXdDLHlCQUF5QixnREFBZ0QscUJBQXFCLGlCQUFpQiw2Q0FBNkMsNEJBQTRCLHVDQUF1Qyx3Q0FBd0MseUJBQXlCLGdEQUFnRCxxQkFBcUIsaUJBQWlCLGFBQWEsU0FBUyxLQUFLLG1CQUFtQjs7QUFFNXdWOzs7Ozs7O0FDUEE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1DQUFtQyxnQkFBZ0I7QUFDbkQsSUFBSTtBQUNKO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGdCQUFnQixpQkFBaUI7QUFDakM7QUFDQTtBQUNBO0FBQ0E7QUFDQSxZQUFZLG9CQUFvQjtBQUNoQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRzs7QUFFSDtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxvREFBb0QsY0FBYzs7QUFFbEU7QUFDQTs7Ozs7OztBQzNFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOztBQUVEO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLENBQUM7QUFDRDtBQUNBLENBQUM7O0FBRUQ7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTs7QUFFQSxpQkFBaUIsbUJBQW1CO0FBQ3BDO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLGlCQUFpQixzQkFBc0I7QUFDdkM7O0FBRUE7QUFDQSxtQkFBbUIsMkJBQTJCOztBQUU5QztBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0EsZ0JBQWdCLG1CQUFtQjtBQUNuQztBQUNBOztBQUVBO0FBQ0E7O0FBRUEsaUJBQWlCLDJCQUEyQjtBQUM1QztBQUNBOztBQUVBLFFBQVEsdUJBQXVCO0FBQy9CO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7O0FBRUEsaUJBQWlCLHVCQUF1QjtBQUN4QztBQUNBOztBQUVBLDJCQUEyQjtBQUMzQjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBLGdCQUFnQixpQkFBaUI7QUFDakM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGNBQWM7O0FBRWQsa0RBQWtELHNCQUFzQjtBQUN4RTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBLEVBQUU7QUFDRjtBQUNBLEVBQUU7QUFDRjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxFQUFFO0FBQ0Y7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxNQUFNO0FBQ047QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQTtBQUNBOztBQUVBLEVBQUU7QUFDRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxFQUFFO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxFQUFFO0FBQ0Y7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSx1REFBdUQ7QUFDdkQ7O0FBRUEsNkJBQTZCLG1CQUFtQjs7QUFFaEQ7O0FBRUE7O0FBRUE7QUFDQTs7Ozs7Ozs7QUMvVkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esd0NBQXdDLFdBQVcsRUFBRTtBQUNyRCx3Q0FBd0MsV0FBVyxFQUFFOztBQUVyRDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLHNDQUFzQztBQUN0QyxHQUFHO0FBQ0g7QUFDQSw4REFBOEQ7QUFDOUQ7O0FBRUE7QUFDQTtBQUNBLEVBQUU7O0FBRUY7QUFDQTtBQUNBOzs7Ozs7O0FDeEZBLHFLOzs7Ozs7QUNBQSxxSyIsImZpbGUiOiJhcHAuanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBpbnN0YWxsIGEgSlNPTlAgY2FsbGJhY2sgZm9yIGNodW5rIGxvYWRpbmdcbiBcdHZhciBwYXJlbnRKc29ucEZ1bmN0aW9uID0gd2luZG93W1wid2VicGFja0pzb25wXCJdO1xuIFx0d2luZG93W1wid2VicGFja0pzb25wXCJdID0gZnVuY3Rpb24gd2VicGFja0pzb25wQ2FsbGJhY2soY2h1bmtJZHMsIG1vcmVNb2R1bGVzLCBleGVjdXRlTW9kdWxlcykge1xuIFx0XHQvLyBhZGQgXCJtb3JlTW9kdWxlc1wiIHRvIHRoZSBtb2R1bGVzIG9iamVjdCxcbiBcdFx0Ly8gdGhlbiBmbGFnIGFsbCBcImNodW5rSWRzXCIgYXMgbG9hZGVkIGFuZCBmaXJlIGNhbGxiYWNrXG4gXHRcdHZhciBtb2R1bGVJZCwgY2h1bmtJZCwgaSA9IDAsIHJlc29sdmVzID0gW10sIHJlc3VsdDtcbiBcdFx0Zm9yKDtpIDwgY2h1bmtJZHMubGVuZ3RoOyBpKyspIHtcbiBcdFx0XHRjaHVua0lkID0gY2h1bmtJZHNbaV07XG4gXHRcdFx0aWYoaW5zdGFsbGVkQ2h1bmtzW2NodW5rSWRdKSB7XG4gXHRcdFx0XHRyZXNvbHZlcy5wdXNoKGluc3RhbGxlZENodW5rc1tjaHVua0lkXVswXSk7XG4gXHRcdFx0fVxuIFx0XHRcdGluc3RhbGxlZENodW5rc1tjaHVua0lkXSA9IDA7XG4gXHRcdH1cbiBcdFx0Zm9yKG1vZHVsZUlkIGluIG1vcmVNb2R1bGVzKSB7XG4gXHRcdFx0aWYoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG1vcmVNb2R1bGVzLCBtb2R1bGVJZCkpIHtcbiBcdFx0XHRcdG1vZHVsZXNbbW9kdWxlSWRdID0gbW9yZU1vZHVsZXNbbW9kdWxlSWRdO1xuIFx0XHRcdH1cbiBcdFx0fVxuIFx0XHRpZihwYXJlbnRKc29ucEZ1bmN0aW9uKSBwYXJlbnRKc29ucEZ1bmN0aW9uKGNodW5rSWRzLCBtb3JlTW9kdWxlcywgZXhlY3V0ZU1vZHVsZXMpO1xuIFx0XHR3aGlsZShyZXNvbHZlcy5sZW5ndGgpIHtcbiBcdFx0XHRyZXNvbHZlcy5zaGlmdCgpKCk7XG4gXHRcdH1cblxuIFx0fTtcblxuIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcblxuIFx0Ly8gb2JqZWN0cyB0byBzdG9yZSBsb2FkZWQgYW5kIGxvYWRpbmcgY2h1bmtzXG4gXHR2YXIgaW5zdGFsbGVkQ2h1bmtzID0ge1xuIFx0XHQyOiAwXG4gXHR9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cbiBcdC8vIFRoaXMgZmlsZSBjb250YWlucyBvbmx5IHRoZSBlbnRyeSBjaHVuay5cbiBcdC8vIFRoZSBjaHVuayBsb2FkaW5nIGZ1bmN0aW9uIGZvciBhZGRpdGlvbmFsIGNodW5rc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5lID0gZnVuY3Rpb24gcmVxdWlyZUVuc3VyZShjaHVua0lkKSB7XG4gXHRcdHZhciBpbnN0YWxsZWRDaHVua0RhdGEgPSBpbnN0YWxsZWRDaHVua3NbY2h1bmtJZF07XG4gXHRcdGlmKGluc3RhbGxlZENodW5rRGF0YSA9PT0gMCkge1xuIFx0XHRcdHJldHVybiBuZXcgUHJvbWlzZShmdW5jdGlvbihyZXNvbHZlKSB7IHJlc29sdmUoKTsgfSk7XG4gXHRcdH1cblxuIFx0XHQvLyBhIFByb21pc2UgbWVhbnMgXCJjdXJyZW50bHkgbG9hZGluZ1wiLlxuIFx0XHRpZihpbnN0YWxsZWRDaHVua0RhdGEpIHtcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkQ2h1bmtEYXRhWzJdO1xuIFx0XHR9XG5cbiBcdFx0Ly8gc2V0dXAgUHJvbWlzZSBpbiBjaHVuayBjYWNoZVxuIFx0XHR2YXIgcHJvbWlzZSA9IG5ldyBQcm9taXNlKGZ1bmN0aW9uKHJlc29sdmUsIHJlamVjdCkge1xuIFx0XHRcdGluc3RhbGxlZENodW5rRGF0YSA9IGluc3RhbGxlZENodW5rc1tjaHVua0lkXSA9IFtyZXNvbHZlLCByZWplY3RdO1xuIFx0XHR9KTtcbiBcdFx0aW5zdGFsbGVkQ2h1bmtEYXRhWzJdID0gcHJvbWlzZTtcblxuIFx0XHQvLyBzdGFydCBjaHVuayBsb2FkaW5nXG4gXHRcdHZhciBoZWFkID0gZG9jdW1lbnQuZ2V0RWxlbWVudHNCeVRhZ05hbWUoJ2hlYWQnKVswXTtcbiBcdFx0dmFyIHNjcmlwdCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ3NjcmlwdCcpO1xuIFx0XHRzY3JpcHQudHlwZSA9ICd0ZXh0L2phdmFzY3JpcHQnO1xuIFx0XHRzY3JpcHQuY2hhcnNldCA9ICd1dGYtOCc7XG4gXHRcdHNjcmlwdC5hc3luYyA9IHRydWU7XG4gXHRcdHNjcmlwdC50aW1lb3V0ID0gMTIwMDAwO1xuXG4gXHRcdGlmIChfX3dlYnBhY2tfcmVxdWlyZV9fLm5jKSB7XG4gXHRcdFx0c2NyaXB0LnNldEF0dHJpYnV0ZShcIm5vbmNlXCIsIF9fd2VicGFja19yZXF1aXJlX18ubmMpO1xuIFx0XHR9XG4gXHRcdHNjcmlwdC5zcmMgPSBfX3dlYnBhY2tfcmVxdWlyZV9fLnAgKyBcIlwiICsgKHtcIjBcIjpcImxpYi9sb2Rhc2hcIixcIjFcIjpcImNvbXBvbmVudHMvYXBsaGFiZXRcIn1bY2h1bmtJZF18fGNodW5rSWQpICsgXCIuanNcIjtcbiBcdFx0dmFyIHRpbWVvdXQgPSBzZXRUaW1lb3V0KG9uU2NyaXB0Q29tcGxldGUsIDEyMDAwMCk7XG4gXHRcdHNjcmlwdC5vbmVycm9yID0gc2NyaXB0Lm9ubG9hZCA9IG9uU2NyaXB0Q29tcGxldGU7XG4gXHRcdGZ1bmN0aW9uIG9uU2NyaXB0Q29tcGxldGUoKSB7XG4gXHRcdFx0Ly8gYXZvaWQgbWVtIGxlYWtzIGluIElFLlxuIFx0XHRcdHNjcmlwdC5vbmVycm9yID0gc2NyaXB0Lm9ubG9hZCA9IG51bGw7XG4gXHRcdFx0Y2xlYXJUaW1lb3V0KHRpbWVvdXQpO1xuIFx0XHRcdHZhciBjaHVuayA9IGluc3RhbGxlZENodW5rc1tjaHVua0lkXTtcbiBcdFx0XHRpZihjaHVuayAhPT0gMCkge1xuIFx0XHRcdFx0aWYoY2h1bmspIHtcbiBcdFx0XHRcdFx0Y2h1bmtbMV0obmV3IEVycm9yKCdMb2FkaW5nIGNodW5rICcgKyBjaHVua0lkICsgJyBmYWlsZWQuJykpO1xuIFx0XHRcdFx0fVxuIFx0XHRcdFx0aW5zdGFsbGVkQ2h1bmtzW2NodW5rSWRdID0gdW5kZWZpbmVkO1xuIFx0XHRcdH1cbiBcdFx0fTtcbiBcdFx0aGVhZC5hcHBlbmRDaGlsZChzY3JpcHQpO1xuXG4gXHRcdHJldHVybiBwcm9taXNlO1xuIFx0fTtcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHtcbiBcdFx0XHRcdGNvbmZpZ3VyYWJsZTogZmFsc2UsXG4gXHRcdFx0XHRlbnVtZXJhYmxlOiB0cnVlLFxuIFx0XHRcdFx0Z2V0OiBnZXR0ZXJcbiBcdFx0XHR9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJcIjtcblxuIFx0Ly8gb24gZXJyb3IgZnVuY3Rpb24gZm9yIGFzeW5jIGxvYWRpbmdcbiBcdF9fd2VicGFja19yZXF1aXJlX18ub2UgPSBmdW5jdGlvbihlcnIpIHsgY29uc29sZS5lcnJvcihlcnIpOyB0aHJvdyBlcnI7IH07XG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gMCk7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gd2VicGFjay9ib290c3RyYXAgZTc2Mzg3OTcyOGJiOGNjYWE5ZTYiLCIoIGFzeW5jICgpID0+IHtcclxuICAgIHJlcXVpcmUoJy4vc2Nzcy9pbmRleC5zY3NzJyk7XHJcbiAgICBcclxuICAgIGxldCBfID0gYXdhaXQgcmVxdWlyZSgnLi9saWIvbG9kYXNoJyk7XHJcbiAgICBsZXQgeyBhbHBoYWJldF9lbmNvZGUsIGFscGhhYmV0X2RlY29kZX0gPSBhd2FpdCByZXF1aXJlKCcuL2NvbXBvbmVudHMvYWxwaGFiZXQnKTtcclxuXHJcbiAgICBsZXQgQ2hlc3MgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgdGhpcy5zdG9uZXMgPSBmYWxzZTtcclxuICAgICAgICB0aGlzLmhpc3RvcnkgPSBbXTtcclxuXHJcbiAgICAgICAgbGV0IGF4bGUgPSBbXTtcclxuICAgICAgICBmb3IoIGxldCBpID0gMDsgaSA8IDE1OyBpKysgKSB7XHJcbiAgICAgICAgICAgIGxldCByb3cgPSBbXTtcclxuICAgICAgICAgICAgZm9yKCBsZXQgbiA9IDA7IG4gPCAxNTsgbisrICkge1xyXG4gICAgICAgICAgICAgICAgcm93W25dID0gbnVsbDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBheGxlW2ldID0gcm93O1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLmNoZXNzYm9hcmQgPSAgYXhsZTtcclxuICAgIH07XHJcbiAgICBcclxuICAgIENoZXNzLnByb3RvdHlwZSA9IHtcclxuICAgICAgICBjdXJyZW50U3RvbmVzICgpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRoaXMuc3RvbmVzPyAnd2hpdGUnOiAnYmxhY2snO1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgYWRkU3RlcCAoIHgsIHkpIHtcclxuICAgICAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgICAgIGlmKCBfLnNvbWUoIHRoaXMuaGlzdG9yeSwgeyB4LCB5fSkpe1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgbGV0IHN0b25lcyA9IHRoaXMuY3VycmVudFN0b25lcygpO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuY2hlc3Nib2FyZFsgYWxwaGFiZXRfZGVjb2RlKHgpIC0gMV0gW3kgLSAxXSA9IHN0b25lcztcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmhpc3RvcnkucHVzaCggeyB4LCB5LCBzdG9uZXN9KTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnN0b25lcyA9ICF0aGlzLnN0b25lcztcclxuICAgICAgICAgICAgICAgICAgICBsZXQgY291bnQgPSAwO1xyXG4gICAgICAgICAgICAgICAgICAgIF8ubWFwKCB0aGlzLmhpc3RvcnksIHYgPT4geyBcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYoIHYuc3RvbmVzID09IHN0b25lcyl7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb3VudCsrO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiB2O1xyXG4gICAgICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGNvdW50O1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9IGNhdGNoIChlcnJvcikge1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS5lcnJvciggZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSxcclxuICAgICAgICBzaG93U3RlcCAoKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmhpc3Rvcnk7XHJcbiAgICAgICAgfSxcclxuICAgICAgICBjaGVja1dpbiAoKSB7XHJcbiAgICAgICAgICAgIC8vICDojrflvpfmnIDlkI7kuIDmrKHokL3lrZBcclxuICAgICAgICAgICAgbGV0IHsgeCwgeSwgc3RvbmVzfSA9IF8ubGFzdCggdGhpcy5oaXN0b3J5KTtcclxuICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKCB4LCB5KTtcclxuICAgICAgICAgICAgeCA9IE51bWJlciggYWxwaGFiZXRfZGVjb2RlKCB4KSk7XHJcbiAgICAgICAgICAgIHkgPSBOdW1iZXIoIHkpO1xyXG5cclxuICAgICAgICAgICAgbGV0IGFuZ2xlID0ge1xyXG4gICAgICAgICAgICAgICAgMDogdGhpcy5nZXRDb3VudDAoIHgsIHksIHN0b25lcyksXHJcbiAgICAgICAgICAgICAgICA0NTogdGhpcy5nZXRDb3VudDQ1KCB4LCB5LCBzdG9uZXMpLFxyXG4gICAgICAgICAgICAgICAgOTA6IHRoaXMuZ2V0Q291bnQ5MCggeCwgeSwgc3RvbmVzKSxcclxuICAgICAgICAgICAgICAgIDEzNTogdGhpcy5nZXRDb3VudDEzNSggeCwgeSwgc3RvbmVzKSxcclxuICAgICAgICAgICAgICAgIDE4MDogdGhpcy5nZXRDb3VudDE4MCggeCwgeSwgc3RvbmVzKSxcclxuICAgICAgICAgICAgICAgIDIyNTogdGhpcy5nZXRDb3VudDIyNSggeCwgeSwgc3RvbmVzKSxcclxuICAgICAgICAgICAgICAgIDI3MDogdGhpcy5nZXRDb3VudDI3MCggeCwgeSwgc3RvbmVzKSxcclxuICAgICAgICAgICAgICAgIDMxNTogdGhpcy5nZXRDb3VudDMxNSggeCwgeSwgc3RvbmVzKVxyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICBhbmdsZS5fMSA9IGFuZ2xlWzBdICsgYW5nbGVbMTgwXSAtIDE7XHJcbiAgICAgICAgICAgIGFuZ2xlLl8yID0gYW5nbGVbNDVdICsgYW5nbGVbMjI1XSAtIDE7XHJcbiAgICAgICAgICAgIGFuZ2xlLl8zID0gYW5nbGVbOTBdICsgYW5nbGVbMjcwXSAtIDE7XHJcbiAgICAgICAgICAgIGFuZ2xlLl80ID0gYW5nbGVbMTM1XSArIGFuZ2xlWzMxNV0gLSAxO1xyXG5cclxuICAgICAgICAgICAgY29uc29sZS5sb2coIGFuZ2xlKTtcclxuICAgICAgICAgICAgaWYgKCBfLnNvbWUoIGFuZ2xlLCAgdiA9PiB2ID49IDUpKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgaXNFcXVhbCAoIHgsIHksIHN0b25lcykge1xyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5jaGVzc2JvYXJkW3gtMV1beS0xXSAhPSBzdG9uZXM7XHJcbiAgICAgICAgfSxcclxuICAgICAgICBnZXRDb3VudDAoIHgsIHksIHN0b25lcykge1xyXG4gICAgICAgICAgICBsZXQgY291bnQgPSAxO1xyXG4gICAgICAgICAgICB5IC09IDE7XHJcbiAgICAgICAgICAgIGlmICggeSA8IDEgfHwgdGhpcy5pc0VxdWFsKCB4LCB5LCBzdG9uZXMpKXtcclxuICAgICAgICAgICAgICAgIHJldHVybiBjb3VudDtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBjb3VudCArIHRoaXMuZ2V0Q291bnQwKCB4LCB5LCBzdG9uZXMpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSxcclxuICAgICAgICBnZXRDb3VudDQ1KCB4LCB5LCBzdG9uZXMpIHtcclxuICAgICAgICAgICAgbGV0IGNvdW50ID0gMTtcclxuICAgICAgICAgICAgeCArPSAxO1xyXG4gICAgICAgICAgICB5IC09IDE7XHJcbiAgICAgICAgICAgIGlmICggeCA+IDE1IHx8IHkgPCAxIHx8IHRoaXMuaXNFcXVhbCggeCwgeSwgc3RvbmVzKSl7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gY291bnQ7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gY291bnQgKyB0aGlzLmdldENvdW50NDUoIHgsIHksIHN0b25lcyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9LFxyXG4gICAgICAgIGdldENvdW50OTAoIHgsIHksIHN0b25lcykge1xyXG4gICAgICAgICAgICBsZXQgY291bnQgPSAxO1xyXG4gICAgICAgICAgICB4ICs9IDE7XHJcbiAgICAgICAgICAgIGlmICggeCA+IDE1IHx8IHRoaXMuaXNFcXVhbCggeCwgeSwgc3RvbmVzKSl7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gY291bnQ7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gY291bnQgKyB0aGlzLmdldENvdW50OTAoIHgsIHksIHN0b25lcyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9LFxyXG4gICAgICAgIGdldENvdW50MTM1KCB4LCB5LCBzdG9uZXMpIHtcclxuICAgICAgICAgICAgbGV0IGNvdW50ID0gMTtcclxuICAgICAgICAgICAgeCArPSAxO1xyXG4gICAgICAgICAgICB5ICs9IDE7XHJcbiAgICAgICAgICAgIGlmICggeCA+IDE1IHx8IHkgPiAxNSB8fCB0aGlzLmlzRXF1YWwoIHgsIHksIHN0b25lcykpe1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGNvdW50O1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGNvdW50ICsgdGhpcy5nZXRDb3VudDEzNSggeCwgeSwgc3RvbmVzKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgZ2V0Q291bnQxODAoIHgsIHksIHN0b25lcykge1xyXG4gICAgICAgICAgICBsZXQgY291bnQgPSAxO1xyXG4gICAgICAgICAgICB5ICs9IDE7XHJcbiAgICAgICAgICAgIGlmICggeSA+IDE1IHx8IHRoaXMuaXNFcXVhbCggeCwgeSwgc3RvbmVzKSl7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gY291bnQ7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gY291bnQgKyB0aGlzLmdldENvdW50MTgwKCB4LCB5LCBzdG9uZXMpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSxcclxuICAgICAgICBnZXRDb3VudDIyNSggeCwgeSwgc3RvbmVzKSB7XHJcbiAgICAgICAgICAgIGxldCBjb3VudCA9IDE7XHJcbiAgICAgICAgICAgIHkgKz0gMTtcclxuICAgICAgICAgICAgeCAtPSAxO1xyXG4gICAgICAgICAgICBpZiAoIHggPCAxIHx8IHkgPiAxNSB8fCB0aGlzLmlzRXF1YWwoIHgsIHksIHN0b25lcykpe1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGNvdW50O1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGNvdW50ICsgdGhpcy5nZXRDb3VudDIyNSggeCwgeSwgc3RvbmVzKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgZ2V0Q291bnQyNzAoIHgsIHksIHN0b25lcykge1xyXG4gICAgICAgICAgICBsZXQgY291bnQgPSAxO1xyXG4gICAgICAgICAgICB4IC09IDE7XHJcbiAgICAgICAgICAgIGlmICggeCA8IDEgfHwgdGhpcy5pc0VxdWFsKCB4LCB5LCBzdG9uZXMpKXtcclxuICAgICAgICAgICAgICAgIHJldHVybiBjb3VudDtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBjb3VudCArIHRoaXMuZ2V0Q291bnQyNzAoIHgsIHksIHN0b25lcyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9LFxyXG4gICAgICAgIGdldENvdW50MzE1KCB4LCB5LCBzdG9uZXMpIHtcclxuICAgICAgICAgICAgbGV0IGNvdW50ID0gMTtcclxuICAgICAgICAgICAgeCAtPSAxO1xyXG4gICAgICAgICAgICB5IC09IDE7XHJcbiAgICAgICAgICAgIGlmICggeCA8IDEgfHwgeSA8IDEgfHwgdGhpcy5pc0VxdWFsKCB4LCB5LCBzdG9uZXMpKXtcclxuICAgICAgICAgICAgICAgIHJldHVybiBjb3VudDtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBjb3VudCArIHRoaXMuZ2V0Q291bnQzMTUoIHgsIHksIHN0b25lcyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9O1xyXG5cclxuICAgIGxldCBjaGVzcyA9IG5ldyBDaGVzcygpO1xyXG5cclxuICAgIGxldCBjaGVzc2JvYXJkID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnc2VjdGlvbicpO1xyXG4gICAgY2hlc3Nib2FyZC5zZXRBdHRyaWJ1dGUoICdpZCcsICdjaGVzc2JvYXJkJyk7XHJcbiAgICBsZXQgY2hlc3Nib3ggPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKTtcclxuICAgIGNoZXNzYm94LnNldEF0dHJpYnV0ZSggJ2lkJywgJ2NoZXNzYm94Jyk7XHJcbiAgICBsZXQgc2NhbGVwbGF0ZVggPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKTtcclxuICAgIHNjYWxlcGxhdGVYLnNldEF0dHJpYnV0ZSggJ2lkJywgJ3NjYWxlcGxhdGVYJyk7XHJcbiAgICBsZXQgc2NhbGVwbGF0ZVkgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKTtcclxuICAgIHNjYWxlcGxhdGVZLnNldEF0dHJpYnV0ZSggJ2lkJywgJ3NjYWxlcGxhdGVZJyk7XHJcbiAgICBcclxuICAgIFxyXG4gICAgZm9yICggbGV0IHkgPSAxOyB5IDw9IDE1OyB5KyspIHtcclxuICAgICAgICBsZXQgbGFiZWxZID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZGl2Jyk7XHJcbiAgICAgICAgbGFiZWxZLnNldEF0dHJpYnV0ZSggJ2NsYXNzJywgJ2xhYmVsJyk7XHJcbiAgICAgICAgbGFiZWxZLmlubmVyVGV4dCA9IHk7XHJcbiAgICAgICAgc2NhbGVwbGF0ZVkuYXBwZW5kQ2hpbGQoIGxhYmVsWSk7XHJcblxyXG4gICAgICAgIGxldCBsYWJlbFggPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKTtcclxuICAgICAgICBsYWJlbFguc2V0QXR0cmlidXRlKCAnY2xhc3MnLCAnbGFiZWwnKTtcclxuICAgICAgICBsYWJlbFguaW5uZXJUZXh0ID0gYWxwaGFiZXRfZW5jb2RlKCB5KTtcclxuICAgICAgICBzY2FsZXBsYXRlWC5hcHBlbmRDaGlsZCggbGFiZWxYKTtcclxuXHJcbiAgICAgICAgZm9yICggbGV0IHggPSAxOyB4IDw9MTU7IHgrKykge1xyXG4gICAgICAgICAgICBsZXQgZ3JpZCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpO1xyXG4gICAgICAgICAgICBncmlkLnNldEF0dHJpYnV0ZSggJ2NsYXNzJywgJ2dyaWQnKTtcclxuICAgICAgICAgICAgZ3JpZC5zZXRBdHRyaWJ1dGUoICdkYXRhLXgnLCBhbHBoYWJldF9lbmNvZGUoIHgpKTtcclxuICAgICAgICAgICAgZ3JpZC5zZXRBdHRyaWJ1dGUoICdkYXRhLXknLCB5KTtcclxuICAgICAgICAgICAgZ3JpZC5zZXRBdHRyaWJ1dGUoICdkYXRhLXN0YXR1cycsIG51bGwpO1xyXG5cclxuICAgICAgICAgICAgbGV0IHBpZWNlID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZGl2Jyk7XHJcbiAgICAgICAgICAgIHBpZWNlLnNldEF0dHJpYnV0ZSggJ2NsYXNzJywgJ3BpZWNlJyk7XHJcbiAgICAgICAgICAgIGxldCBwaWVjZV9jb250ZW50ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZGl2Jyk7XHJcbiAgICAgICAgICAgIHBpZWNlX2NvbnRlbnQuc2V0QXR0cmlidXRlKCAnY2xhc3MnLCAncGllY2VfY29udGVudCcpO1xyXG4gICAgICAgICAgICBwaWVjZS5hcHBlbmRDaGlsZCggcGllY2VfY29udGVudCk7XHJcbiAgICAgICAgICAgIGdyaWQuYXBwZW5kQ2hpbGQoIHBpZWNlKTtcclxuICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIGdyaWQub25jbGljayA9IGUgPT4ge1xyXG4gICAgICAgICAgICAgICAgbGV0IF90aGlzID0gZS5jdXJyZW50VGFyZ2V0O1xyXG4gICAgICAgICAgICAgICAgbGV0IHggPSBfdGhpcy5nZXRBdHRyaWJ1dGUoJ2RhdGEteCcpLFxyXG4gICAgICAgICAgICAgICAgICAgIHkgPSBfdGhpcy5nZXRBdHRyaWJ1dGUoJ2RhdGEteScpO1xyXG5cclxuICAgICAgICAgICAgICAgIGxldCBzdG9uZXMgPSBjaGVzcy5jdXJyZW50U3RvbmVzKCk7XHJcbiAgICAgICAgICAgICAgICBsZXQgc3RlcCA9IGNoZXNzLmFkZFN0ZXAoIHgsIHkpO1xyXG4gICAgICAgICAgICAgICAgaWYgKCBfLmlzTnVtYmVyKCBzdGVwKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIF90aGlzLnNldEF0dHJpYnV0ZSgnZGF0YS1zdGF0dXMnLCBzdG9uZXMpO1xyXG4gICAgICAgICAgICAgICAgICAgIHBpZWNlX2NvbnRlbnQuaW5uZXJUZXh0ID0gc3RlcDtcclxuICAgICAgICAgICAgICAgICAgICBzZXRUaW1lb3V0KCAoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmKCBjaGVzcy5jaGVja1dpbigpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBsZXQgbmFtZSA9IHN0b25lcyA9PSAnd2hpdGUnPyAn55m9JzogJ+m7kSc7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBhbGVydCggbmFtZSsn5pa56I636IOc77yBJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9LCAxOCk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIGFsZXJ0KCAn5oCO5LmI5Y+v5Lul5LiL5Yir5Lq65LiL6L+H55qE5Zyw5pa577yfJyk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGNoZXNzYm94LmFwcGVuZENoaWxkKCBncmlkKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICBjaGVzc2JvYXJkLmFwcGVuZENoaWxkKCBzY2FsZXBsYXRlWCk7XHJcbiAgICBjaGVzc2JvYXJkLmFwcGVuZENoaWxkKCBzY2FsZXBsYXRlWSk7XHJcbiAgICBjaGVzc2JvYXJkLmFwcGVuZENoaWxkKCBjaGVzc2JveCk7XHJcblxyXG4gICAgZG9jdW1lbnQuYm9keS5hcHBlbmRDaGlsZCggY2hlc3Nib2FyZCk7XHJcbn0pKClcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3NyYy9tYWluLmpzXG4vLyBtb2R1bGUgaWQgPSAwXG4vLyBtb2R1bGUgY2h1bmtzID0gMiIsIi8vIHN0eWxlLWxvYWRlcjogQWRkcyBzb21lIGNzcyB0byB0aGUgRE9NIGJ5IGFkZGluZyBhIDxzdHlsZT4gdGFnXG5cbi8vIGxvYWQgdGhlIHN0eWxlc1xudmFyIGNvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi9ub2RlX21vZHVsZXMvX2Nzcy1sb2FkZXJAMC4yOC43QGNzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tMC0xIS4uLy4uL25vZGVfbW9kdWxlcy9fc2Fzcy1sb2FkZXJANi4wLjZAc2Fzcy1sb2FkZXIvbGliL2xvYWRlci5qcz8/cmVmLS0wLTIhLi9pbmRleC5zY3NzXCIpO1xuaWYodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG4vLyBQcmVwYXJlIGNzc1RyYW5zZm9ybWF0aW9uXG52YXIgdHJhbnNmb3JtO1xuXG52YXIgb3B0aW9ucyA9IHt9XG5vcHRpb25zLnRyYW5zZm9ybSA9IHRyYW5zZm9ybVxuLy8gYWRkIHRoZSBzdHlsZXMgdG8gdGhlIERPTVxudmFyIHVwZGF0ZSA9IHJlcXVpcmUoXCIhLi4vLi4vbm9kZV9tb2R1bGVzL19zdHlsZS1sb2FkZXJAMC4xOC4yQHN0eWxlLWxvYWRlci9saWIvYWRkU3R5bGVzLmpzXCIpKGNvbnRlbnQsIG9wdGlvbnMpO1xuaWYoY29udGVudC5sb2NhbHMpIG1vZHVsZS5leHBvcnRzID0gY29udGVudC5sb2NhbHM7XG4vLyBIb3QgTW9kdWxlIFJlcGxhY2VtZW50XG5pZihtb2R1bGUuaG90KSB7XG5cdC8vIFdoZW4gdGhlIHN0eWxlcyBjaGFuZ2UsIHVwZGF0ZSB0aGUgPHN0eWxlPiB0YWdzXG5cdGlmKCFjb250ZW50LmxvY2Fscykge1xuXHRcdG1vZHVsZS5ob3QuYWNjZXB0KFwiISEuLi8uLi9ub2RlX21vZHVsZXMvX2Nzcy1sb2FkZXJAMC4yOC43QGNzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tMC0xIS4uLy4uL25vZGVfbW9kdWxlcy9fc2Fzcy1sb2FkZXJANi4wLjZAc2Fzcy1sb2FkZXIvbGliL2xvYWRlci5qcz8/cmVmLS0wLTIhLi9pbmRleC5zY3NzXCIsIGZ1bmN0aW9uKCkge1xuXHRcdFx0dmFyIG5ld0NvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi9ub2RlX21vZHVsZXMvX2Nzcy1sb2FkZXJAMC4yOC43QGNzcy1sb2FkZXIvaW5kZXguanM/P3JlZi0tMC0xIS4uLy4uL25vZGVfbW9kdWxlcy9fc2Fzcy1sb2FkZXJANi4wLjZAc2Fzcy1sb2FkZXIvbGliL2xvYWRlci5qcz8/cmVmLS0wLTIhLi9pbmRleC5zY3NzXCIpO1xuXHRcdFx0aWYodHlwZW9mIG5ld0NvbnRlbnQgPT09ICdzdHJpbmcnKSBuZXdDb250ZW50ID0gW1ttb2R1bGUuaWQsIG5ld0NvbnRlbnQsICcnXV07XG5cdFx0XHR1cGRhdGUobmV3Q29udGVudCk7XG5cdFx0fSk7XG5cdH1cblx0Ly8gV2hlbiB0aGUgbW9kdWxlIGlzIGRpc3Bvc2VkLCByZW1vdmUgdGhlIDxzdHlsZT4gdGFnc1xuXHRtb2R1bGUuaG90LmRpc3Bvc2UoZnVuY3Rpb24oKSB7IHVwZGF0ZSgpOyB9KTtcbn1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3NyYy9zY3NzL2luZGV4LnNjc3Ncbi8vIG1vZHVsZSBpZCA9IDFcbi8vIG1vZHVsZSBjaHVua3MgPSAyIiwiZXhwb3J0cyA9IG1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIi4uLy4uL25vZGVfbW9kdWxlcy9fY3NzLWxvYWRlckAwLjI4LjdAY3NzLWxvYWRlci9saWIvY3NzLWJhc2UuanNcIikodHJ1ZSk7XG4vLyBpbXBvcnRzXG5cblxuLy8gbW9kdWxlXG5leHBvcnRzLnB1c2goW21vZHVsZS5pZCwgXCIqIHtcXG4gIG1hcmdpbjogMDtcXG4gIHBhZGRpbmc6IDA7IH1cXG5cXG5odG1sLFxcbmJvZHkge1xcbiAgd2lkdGg6IDEwMCU7XFxuICBoZWlnaHQ6IDEwMCU7IH1cXG5cXG4jY2hlc3Nib2FyZCB7XFxuICB3aWR0aDogNzUwcHg7XFxuICBoZWlnaHQ6IDc1MHB4O1xcbiAgcGFkZGluZzogMTVweDtcXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTsgfVxcbiAgI2NoZXNzYm9hcmQgI3NjYWxlcGxhdGVYLFxcbiAgI2NoZXNzYm9hcmQgI3NjYWxlcGxhdGVZIHtcXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgICB0b3A6IDA7XFxuICAgIGxlZnQ6IDA7XFxuICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7IH1cXG4gICAgI2NoZXNzYm9hcmQgI3NjYWxlcGxhdGVYIC5sYWJlbCxcXG4gICAgI2NoZXNzYm9hcmQgI3NjYWxlcGxhdGVZIC5sYWJlbCB7XFxuICAgICAgd2lkdGg6IDUwcHg7XFxuICAgICAgaGVpZ2h0OiA1MHB4O1xcbiAgICAgIGxpbmUtaGVpZ2h0OiA1MHB4O1xcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgICBmbG9hdDogbGVmdDsgfVxcbiAgI2NoZXNzYm9hcmQgI3NjYWxlcGxhdGVYIHtcXG4gICAgaGVpZ2h0OiAzNXB4O1xcbiAgICByaWdodDogMDtcXG4gICAgcGFkZGluZzogMCAxNXB4OyB9XFxuICAgICNjaGVzc2JvYXJkICNzY2FsZXBsYXRlWCAubGFiZWwge1xcbiAgICAgIGhlaWdodDogNDBweDtcXG4gICAgICBsaW5lLWhlaWdodDogNDBweDsgfVxcbiAgI2NoZXNzYm9hcmQgI3NjYWxlcGxhdGVZIHtcXG4gICAgd2lkdGg6IDM1cHg7XFxuICAgIGJvdHRvbTogMDtcXG4gICAgcGFkZGluZzogMTVweCAwOyB9XFxuICAgICNjaGVzc2JvYXJkICNzY2FsZXBsYXRlWSAubGFiZWwge1xcbiAgICAgIHdpZHRoOiA0MHB4O1xcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjsgfVxcbiAgI2NoZXNzYm9hcmQgI2NoZXNzYm94OmFmdGVyLCAjY2hlc3Nib2FyZCAjY2hlc3Nib3g6YmVmb3JlIHtcXG4gICAgZGlzcGxheTogdGFibGU7XFxuICAgIGNvbnRlbnQ6IFxcXCJcXFwiOyB9XFxuICAjY2hlc3Nib2FyZCAjY2hlc3Nib3g6YWZ0ZXIge1xcbiAgICBjbGVhcjogYm90aDsgfVxcbiAgI2NoZXNzYm9hcmQgI2NoZXNzYm94IC5ncmlkIHtcXG4gICAgd2lkdGg6IDUwcHg7XFxuICAgIGhlaWdodDogNTBweDtcXG4gICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcXG4gICAgZmxvYXQ6IGxlZnQ7XFxuICAgIGN1cnNvcjogcG9pbnRlcjtcXG4gICAgcG9zaXRpb246IHJlbGF0aXZlOyB9XFxuICAgICNjaGVzc2JvYXJkICNjaGVzc2JveCAuZ3JpZDphZnRlciwgI2NoZXNzYm9hcmQgI2NoZXNzYm94IC5ncmlkOmJlZm9yZSB7XFxuICAgICAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgICAgIGJhY2tncm91bmQ6ICMwMDA7XFxuICAgICAgY29udGVudDogXFxcIlxcXCI7IH1cXG4gICAgI2NoZXNzYm9hcmQgI2NoZXNzYm94IC5ncmlkOmFmdGVyIHtcXG4gICAgICB3aWR0aDogMnB4O1xcbiAgICAgIGhlaWdodDogMTAwJTtcXG4gICAgICBsZWZ0OiA1MCU7XFxuICAgICAgbWFyZ2luLWxlZnQ6IC0xcHg7IH1cXG4gICAgI2NoZXNzYm9hcmQgI2NoZXNzYm94IC5ncmlkOmJlZm9yZSB7XFxuICAgICAgaGVpZ2h0OiAycHg7XFxuICAgICAgd2lkdGg6IDEwMCU7XFxuICAgICAgdG9wOiA1MCU7XFxuICAgICAgbWFyZ2luLXRvcDogLTFweDsgfVxcbiAgICAjY2hlc3Nib2FyZCAjY2hlc3Nib3ggLmdyaWRbZGF0YS15PVxcXCIxXFxcIl06YWZ0ZXIge1xcbiAgICAgIHRvcDogNTAlOyB9XFxuICAgICNjaGVzc2JvYXJkICNjaGVzc2JveCAuZ3JpZFtkYXRhLXk9XFxcIjE1XFxcIl06YWZ0ZXIge1xcbiAgICAgIGJvdHRvbTogNTAlOyB9XFxuICAgICNjaGVzc2JvYXJkICNjaGVzc2JveCAuZ3JpZFtkYXRhLXg9XFxcIkFcXFwiXTpiZWZvcmUge1xcbiAgICAgIGxlZnQ6IDUwJTsgfVxcbiAgICAjY2hlc3Nib2FyZCAjY2hlc3Nib3ggLmdyaWRbZGF0YS14PVxcXCJPXFxcIl06YmVmb3JlIHtcXG4gICAgICByaWdodDogNTAlOyB9XFxuICAgICNjaGVzc2JvYXJkICNjaGVzc2JveCAuZ3JpZCAucGllY2Uge1xcbiAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gICAgICB6LWluZGV4OiAyO1xcbiAgICAgIHRvcDogMDtcXG4gICAgICBsZWZ0OiAwO1xcbiAgICAgIHJpZ2h0OiAwO1xcbiAgICAgIGJvdHRvbTogMDtcXG4gICAgICBwYWRkaW5nOiAzcHg7XFxuICAgICAgYm94LXNpemluZzogYm9yZGVyLWJveDsgfVxcbiAgICAgICNjaGVzc2JvYXJkICNjaGVzc2JveCAuZ3JpZCAucGllY2UgLnBpZWNlX2NvbnRlbnQge1xcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgICAgICAgei1pbmRleDogMjtcXG4gICAgICAgIHRvcDogMDtcXG4gICAgICAgIGxlZnQ6IDA7XFxuICAgICAgICByaWdodDogMDtcXG4gICAgICAgIGJvdHRvbTogMDtcXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gICAgICAgIGZvbnQtc2l6ZTogMTRweDtcXG4gICAgICAgIGxpbmUtaGVpZ2h0OiA1MHB4OyB9XFxuICAgICAgI2NoZXNzYm9hcmQgI2NoZXNzYm94IC5ncmlkIC5waWVjZTphZnRlciB7XFxuICAgICAgICBjb250ZW50OiAnJztcXG4gICAgICAgIHRvcDogNTAlO1xcbiAgICAgICAgbGVmdDogNTAlO1xcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgICAgICAgYm9yZGVyLXJhZGl1czogNTAlO1xcbiAgICAgICAgYm94LXNpemluZzogYm9yZGVyLWJveDsgfVxcbiAgICAjY2hlc3Nib2FyZCAjY2hlc3Nib3ggLmdyaWRbZGF0YS14PVxcXCJEXFxcIl1bZGF0YS15PVxcXCI0XFxcIl0gLnBpZWNlOmFmdGVyLCAjY2hlc3Nib2FyZCAjY2hlc3Nib3ggLmdyaWRbZGF0YS14PVxcXCJEXFxcIl1bZGF0YS15PVxcXCIxMlxcXCJdIC5waWVjZTphZnRlciwgI2NoZXNzYm9hcmQgI2NoZXNzYm94IC5ncmlkW2RhdGEteD1cXFwiTFxcXCJdW2RhdGEteT1cXFwiNFxcXCJdIC5waWVjZTphZnRlciwgI2NoZXNzYm9hcmQgI2NoZXNzYm94IC5ncmlkW2RhdGEteD1cXFwiTFxcXCJdW2RhdGEteT1cXFwiMTJcXFwiXSAucGllY2U6YWZ0ZXIsICNjaGVzc2JvYXJkICNjaGVzc2JveCAuZ3JpZFtkYXRhLXg9XFxcIkhcXFwiXVtkYXRhLXk9XFxcIjhcXFwiXSAucGllY2U6YWZ0ZXIge1xcbiAgICAgIG1hcmdpbi10b3A6IC03cHg7XFxuICAgICAgbWFyZ2luLWxlZnQ6IC03cHg7XFxuICAgICAgd2lkdGg6IDE0cHg7XFxuICAgICAgaGVpZ2h0OiAxNHB4O1xcbiAgICAgIGJhY2tncm91bmQ6ICMwMDA7IH1cXG4gICAgI2NoZXNzYm9hcmQgI2NoZXNzYm94IC5ncmlkW2RhdGEtc3RhdHVzPVxcXCJ3aGl0ZVxcXCJdIC5waWVjZSAucGllY2VfY29udGVudCB7XFxuICAgICAgY29sb3I6ICMwMDA7IH1cXG4gICAgI2NoZXNzYm9hcmQgI2NoZXNzYm94IC5ncmlkW2RhdGEtc3RhdHVzPVxcXCJ3aGl0ZVxcXCJdIC5waWVjZTphZnRlciB7XFxuICAgICAgbWFyZ2luLXRvcDogLTIzcHggIWltcG9ydGFudDtcXG4gICAgICBtYXJnaW4tbGVmdDogLTIzcHggIWltcG9ydGFudDtcXG4gICAgICB3aWR0aDogNDZweCAhaW1wb3J0YW50O1xcbiAgICAgIGhlaWdodDogNDZweCAhaW1wb3J0YW50O1xcbiAgICAgIGJhY2tncm91bmQ6ICNmZmYgIWltcG9ydGFudDtcXG4gICAgICBib3JkZXI6IDFweCBzb2xpZCAjMDAwICFpbXBvcnRhbnQ7IH1cXG4gICAgI2NoZXNzYm9hcmQgI2NoZXNzYm94IC5ncmlkW2RhdGEtc3RhdHVzPVxcXCJibGFja1xcXCJdIC5waWVjZSAucGllY2VfY29udGVudCB7XFxuICAgICAgY29sb3I6ICNmZmY7IH1cXG4gICAgI2NoZXNzYm9hcmQgI2NoZXNzYm94IC5ncmlkW2RhdGEtc3RhdHVzPVxcXCJibGFja1xcXCJdIC5waWVjZTphZnRlciB7XFxuICAgICAgbWFyZ2luLXRvcDogLTIzcHggIWltcG9ydGFudDtcXG4gICAgICBtYXJnaW4tbGVmdDogLTIzcHggIWltcG9ydGFudDtcXG4gICAgICB3aWR0aDogNDZweCAhaW1wb3J0YW50O1xcbiAgICAgIGhlaWdodDogNDZweCAhaW1wb3J0YW50O1xcbiAgICAgIGJhY2tncm91bmQ6ICMwMDAgIWltcG9ydGFudDtcXG4gICAgICBib3JkZXI6IDFweCBzb2xpZCAjMDAwICFpbXBvcnRhbnQ7IH1cXG5cIiwgXCJcIiwge1widmVyc2lvblwiOjMsXCJzb3VyY2VzXCI6W1wiRzovd3d3L2dvYmFuZy9zcmMvc2Nzcy9zcmMvc2Nzcy9pbmRleC5zY3NzXCJdLFwibmFtZXNcIjpbXSxcIm1hcHBpbmdzXCI6XCJBQUFBO0VBQ0ksVUFBUztFQUNULFdBQVUsRUFDYjs7QUFDRDs7RUFFSSxZQUFXO0VBQ1gsYUFBWSxFQUNmOztBQUVEO0VBQ0ksYUFBbUI7RUFDbkIsY0FBb0I7RUFDcEIsY0FBYTtFQUNiLG1CQUFrQixFQW9NckI7RUF4TUQ7O0lBUVEsbUJBQWtCO0lBQ2xCLE9BQU07SUFDTixRQUFPO0lBQ1AsdUJBQXNCLEVBUXpCO0lBbkJMOztNQWFZLFlBQVc7TUFDWCxhQUFZO01BQ1osa0JBQWlCO01BQ2pCLG1CQUFrQjtNQUNsQixZQUFXLEVBQ2Q7RUFsQlQ7SUFzQlEsYUFBWTtJQUNaLFNBQVE7SUFDUixnQkFBZSxFQUtsQjtJQTdCTDtNQTBCWSxhQUFZO01BQ1osa0JBQWlCLEVBQ3BCO0VBNUJUO0lBZ0NRLFlBQVc7SUFDWCxVQUFTO0lBQ1QsZ0JBQWUsRUFLbEI7SUF2Q0w7TUFvQ1ksWUFBVztNQUNYLG1CQUFrQixFQUNyQjtFQXRDVDtJQTZDWSxlQUFjO0lBQ2QsWUFBVyxFQUNkO0VBL0NUO0lBa0RZLFlBQVcsRUFDZDtFQW5EVDtJQXNEWSxZQUFXO0lBQ1gsYUFBWTtJQUNaLHVCQUFzQjtJQUV0QixZQUFXO0lBQ1gsZ0JBQWU7SUFDZixtQkFBa0IsRUEwSXJCO0lBdE1UO01BZ0VnQixtQkFBa0I7TUFDbEIsaUJBQWdCO01BQ2hCLFlBQVcsRUFDZDtJQW5FYjtNQXNFZ0IsV0FBVTtNQUNWLGFBQVk7TUFDWixVQUFTO01BQ1Qsa0JBQWlCLEVBQ3BCO0lBMUViO01BNkVnQixZQUFXO01BQ1gsWUFBVztNQUNYLFNBQVE7TUFDUixpQkFBZ0IsRUFDbkI7SUFqRmI7TUFxRm9CLFNBQVEsRUFDWDtJQXRGakI7TUEyRm9CLFlBQVcsRUFDZDtJQTVGakI7TUFpR29CLFVBQVMsRUFDWjtJQWxHakI7TUF1R29CLFdBQVUsRUFDYjtJQXhHakI7TUE0R2dCLG1CQUFrQjtNQUNsQixXQUFVO01BQ1YsT0FBTTtNQUNOLFFBQU87TUFDUCxTQUFRO01BQ1IsVUFBUztNQUNULGFBQVk7TUFDWix1QkFBc0IsRUFzQnpCO01BekliO1FBc0hvQixtQkFBa0I7UUFDbEIsV0FBVTtRQUNWLE9BQU07UUFDTixRQUFPO1FBQ1AsU0FBUTtRQUNSLFVBQVM7UUFDVCxtQkFBa0I7UUFDbEIsZ0JBQWU7UUFDZixrQkFBaUIsRUFDcEI7TUEvSGpCO1FBa0lvQixZQUFXO1FBQ1gsU0FBUTtRQUNSLFVBQVM7UUFDVCxtQkFBa0I7UUFDbEIsbUJBQWtCO1FBQ2xCLHVCQUFzQixFQUN6QjtJQXhJakI7TUE2SW9CLGlCQUFnQjtNQUNoQixrQkFBaUI7TUFDakIsWUFBVztNQUNYLGFBQVk7TUFDWixpQkFBZ0IsRUFDbkI7SUFsSmpCO01Bd0x3QixZQUFXLEVBQ2Q7SUF6THJCO01BdUpvQiw2QkFBMkI7TUFDM0IsOEJBQTRCO01BQzVCLHVCQUFxQjtNQUNyQix3QkFBc0I7TUFDdEIsNEJBQTBCO01BQzFCLGtDQUFnQyxFQUNuQztJQTdKakI7TUFpTXdCLFlBQVcsRUFDZDtJQWxNckI7TUFrS29CLDZCQUEyQjtNQUMzQiw4QkFBNEI7TUFDNUIsdUJBQXFCO01BQ3JCLHdCQUFzQjtNQUN0Qiw0QkFBMEI7TUFDMUIsa0NBQWdDLEVBQ25DXCIsXCJmaWxlXCI6XCJpbmRleC5zY3NzXCIsXCJzb3VyY2VzQ29udGVudFwiOltcIioge1xcclxcbiAgICBtYXJnaW46IDA7XFxyXFxuICAgIHBhZGRpbmc6IDA7XFxyXFxufVxcclxcbmh0bWwsXFxyXFxuYm9keSB7XFxyXFxuICAgIHdpZHRoOiAxMDAlO1xcclxcbiAgICBoZWlnaHQ6IDEwMCU7XFxyXFxufVxcclxcblxcclxcbiNjaGVzc2JvYXJkIHtcXHJcXG4gICAgd2lkdGg6IDE1ICogNTAgKyBweDtcXHJcXG4gICAgaGVpZ2h0OiAxNSAqIDUwICsgcHg7XFxyXFxuICAgIHBhZGRpbmc6IDE1cHg7XFxyXFxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcXHJcXG5cXHJcXG4gICAgI3NjYWxlcGxhdGVYLFxcclxcbiAgICAjc2NhbGVwbGF0ZVkge1xcclxcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xcclxcbiAgICAgICAgdG9wOiAwO1xcclxcbiAgICAgICAgbGVmdDogMDtcXHJcXG4gICAgICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XFxyXFxuICAgICAgICAubGFiZWwge1xcclxcbiAgICAgICAgICAgIHdpZHRoOiA1MHB4O1xcclxcbiAgICAgICAgICAgIGhlaWdodDogNTBweDtcXHJcXG4gICAgICAgICAgICBsaW5lLWhlaWdodDogNTBweDtcXHJcXG4gICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxyXFxuICAgICAgICAgICAgZmxvYXQ6IGxlZnQ7XFxyXFxuICAgICAgICB9XFxyXFxuICAgIH1cXHJcXG5cXHJcXG4gICAgI3NjYWxlcGxhdGVYIHtcXHJcXG4gICAgICAgIGhlaWdodDogMzVweDtcXHJcXG4gICAgICAgIHJpZ2h0OiAwO1xcclxcbiAgICAgICAgcGFkZGluZzogMCAxNXB4O1xcclxcbiAgICAgICAgLmxhYmVsIHtcXHJcXG4gICAgICAgICAgICBoZWlnaHQ6IDQwcHg7XFxyXFxuICAgICAgICAgICAgbGluZS1oZWlnaHQ6IDQwcHg7XFxyXFxuICAgICAgICB9XFxyXFxuICAgIH1cXHJcXG5cXHJcXG4gICAgI3NjYWxlcGxhdGVZIHtcXHJcXG4gICAgICAgIHdpZHRoOiAzNXB4O1xcclxcbiAgICAgICAgYm90dG9tOiAwO1xcclxcbiAgICAgICAgcGFkZGluZzogMTVweCAwO1xcclxcbiAgICAgICAgLmxhYmVsIHtcXHJcXG4gICAgICAgICAgICB3aWR0aDogNDBweDtcXHJcXG4gICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxyXFxuICAgICAgICB9XFxyXFxuICAgIH1cXHJcXG5cXHJcXG4gICAgI2NoZXNzYm94IHtcXHJcXG4gICAgICAgIFxcclxcbiAgICAgICAgJjphZnRlcixcXHJcXG4gICAgICAgICY6YmVmb3JlIHtcXHJcXG4gICAgICAgICAgICBkaXNwbGF5OiB0YWJsZTtcXHJcXG4gICAgICAgICAgICBjb250ZW50OiBcXFwiXFxcIjtcXHJcXG4gICAgICAgIH1cXHJcXG5cXHJcXG4gICAgICAgICY6YWZ0ZXIge1xcclxcbiAgICAgICAgICAgIGNsZWFyOiBib3RoO1xcclxcbiAgICAgICAgfVxcclxcblxcclxcbiAgICAgICAgLmdyaWQge1xcclxcbiAgICAgICAgICAgIHdpZHRoOiA1MHB4O1xcclxcbiAgICAgICAgICAgIGhlaWdodDogNTBweDtcXHJcXG4gICAgICAgICAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xcclxcbiAgICAgICAgICAgIFxcclxcbiAgICAgICAgICAgIGZsb2F0OiBsZWZ0O1xcclxcbiAgICAgICAgICAgIGN1cnNvcjogcG9pbnRlcjtcXHJcXG4gICAgICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XFxyXFxuICAgIFxcclxcbiAgICAgICAgICAgICY6YWZ0ZXIsXFxyXFxuICAgICAgICAgICAgJjpiZWZvcmUge1xcclxcbiAgICAgICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XFxyXFxuICAgICAgICAgICAgICAgIGJhY2tncm91bmQ6ICMwMDA7XFxyXFxuICAgICAgICAgICAgICAgIGNvbnRlbnQ6IFxcXCJcXFwiO1xcclxcbiAgICAgICAgICAgIH1cXHJcXG4gICAgXFxyXFxuICAgICAgICAgICAgJjphZnRlciB7XFxyXFxuICAgICAgICAgICAgICAgIHdpZHRoOiAycHg7XFxyXFxuICAgICAgICAgICAgICAgIGhlaWdodDogMTAwJTtcXHJcXG4gICAgICAgICAgICAgICAgbGVmdDogNTAlO1xcclxcbiAgICAgICAgICAgICAgICBtYXJnaW4tbGVmdDogLTFweDtcXHJcXG4gICAgICAgICAgICB9XFxyXFxuICAgIFxcclxcbiAgICAgICAgICAgICY6YmVmb3JlIHtcXHJcXG4gICAgICAgICAgICAgICAgaGVpZ2h0OiAycHg7XFxyXFxuICAgICAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xcclxcbiAgICAgICAgICAgICAgICB0b3A6IDUwJTtcXHJcXG4gICAgICAgICAgICAgICAgbWFyZ2luLXRvcDogLTFweDtcXHJcXG4gICAgICAgICAgICB9XFxyXFxuXFxyXFxuICAgICAgICAgICAgJltkYXRhLXk9XFxcIjFcXFwiXSB7XFxyXFxuICAgICAgICAgICAgICAgICY6YWZ0ZXIge1xcclxcbiAgICAgICAgICAgICAgICAgICAgdG9wOiA1MCU7XFxyXFxuICAgICAgICAgICAgICAgIH1cXHJcXG4gICAgICAgICAgICB9XFxyXFxuICAgICAgICAgICAgXFxyXFxuICAgICAgICAgICAgJltkYXRhLXk9XFxcIjE1XFxcIl0ge1xcclxcbiAgICAgICAgICAgICAgICAmOmFmdGVyIHtcXHJcXG4gICAgICAgICAgICAgICAgICAgIGJvdHRvbTogNTAlO1xcclxcbiAgICAgICAgICAgICAgICB9XFxyXFxuICAgICAgICAgICAgfVxcclxcblxcclxcbiAgICAgICAgICAgICZbZGF0YS14PVxcXCJBXFxcIl0ge1xcclxcbiAgICAgICAgICAgICAgICAmOmJlZm9yZSB7XFxyXFxuICAgICAgICAgICAgICAgICAgICBsZWZ0OiA1MCU7XFxyXFxuICAgICAgICAgICAgICAgIH1cXHJcXG4gICAgICAgICAgICB9XFxyXFxuXFxyXFxuICAgICAgICAgICAgJltkYXRhLXg9XFxcIk9cXFwiXSB7XFxyXFxuICAgICAgICAgICAgICAgICY6YmVmb3JlIHtcXHJcXG4gICAgICAgICAgICAgICAgICAgIHJpZ2h0OiA1MCU7XFxyXFxuICAgICAgICAgICAgICAgIH1cXHJcXG4gICAgICAgICAgICB9XFxyXFxuXFxyXFxuICAgICAgICAgICAgLnBpZWNlIHtcXHJcXG4gICAgICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xcclxcbiAgICAgICAgICAgICAgICB6LWluZGV4OiAyO1xcclxcbiAgICAgICAgICAgICAgICB0b3A6IDA7XFxyXFxuICAgICAgICAgICAgICAgIGxlZnQ6IDA7XFxyXFxuICAgICAgICAgICAgICAgIHJpZ2h0OiAwO1xcclxcbiAgICAgICAgICAgICAgICBib3R0b206IDA7XFxyXFxuICAgICAgICAgICAgICAgIHBhZGRpbmc6IDNweDtcXHJcXG4gICAgICAgICAgICAgICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcXHJcXG5cXHJcXG4gICAgICAgICAgICAgICAgLnBpZWNlX2NvbnRlbnR7XFxyXFxuICAgICAgICAgICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XFxyXFxuICAgICAgICAgICAgICAgICAgICB6LWluZGV4OiAyO1xcclxcbiAgICAgICAgICAgICAgICAgICAgdG9wOiAwO1xcclxcbiAgICAgICAgICAgICAgICAgICAgbGVmdDogMDtcXHJcXG4gICAgICAgICAgICAgICAgICAgIHJpZ2h0OiAwO1xcclxcbiAgICAgICAgICAgICAgICAgICAgYm90dG9tOiAwO1xcclxcbiAgICAgICAgICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xcclxcbiAgICAgICAgICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xcclxcbiAgICAgICAgICAgICAgICAgICAgbGluZS1oZWlnaHQ6IDUwcHg7XFxyXFxuICAgICAgICAgICAgICAgIH1cXHJcXG4gICAgICAgICAgICAgICAgXFxyXFxuICAgICAgICAgICAgICAgICY6YWZ0ZXIge1xcclxcbiAgICAgICAgICAgICAgICAgICAgY29udGVudDogJyc7XFxyXFxuICAgICAgICAgICAgICAgICAgICB0b3A6IDUwJTtcXHJcXG4gICAgICAgICAgICAgICAgICAgIGxlZnQ6IDUwJTtcXHJcXG4gICAgICAgICAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXHJcXG4gICAgICAgICAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcXHJcXG4gICAgICAgICAgICAgICAgICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XFxyXFxuICAgICAgICAgICAgICAgIH1cXHJcXG4gICAgICAgICAgICB9XFxyXFxuXFxyXFxuICAgICAgICAgICAgQG1peGluIHN0YXIgKCkge1xcclxcbiAgICAgICAgICAgICAgICAmOmFmdGVyIHtcXHJcXG4gICAgICAgICAgICAgICAgICAgIG1hcmdpbi10b3A6IC03cHg7XFxyXFxuICAgICAgICAgICAgICAgICAgICBtYXJnaW4tbGVmdDogLTdweDtcXHJcXG4gICAgICAgICAgICAgICAgICAgIHdpZHRoOiAxNHB4O1xcclxcbiAgICAgICAgICAgICAgICAgICAgaGVpZ2h0OiAxNHB4O1xcclxcbiAgICAgICAgICAgICAgICAgICAgYmFja2dyb3VuZDogIzAwMDtcXHJcXG4gICAgICAgICAgICAgICAgfVxcclxcbiAgICAgICAgICAgIH1cXHJcXG5cXHJcXG4gICAgICAgICAgICBAbWl4aW4gd2hpdGUtc3RvbmVzICgpIHtcXHJcXG4gICAgICAgICAgICAgICAgJjphZnRlciB7XFxyXFxuICAgICAgICAgICAgICAgICAgICBtYXJnaW4tdG9wOiAtMjNweCFpbXBvcnRhbnQ7XFxyXFxuICAgICAgICAgICAgICAgICAgICBtYXJnaW4tbGVmdDogLTIzcHghaW1wb3J0YW50O1xcclxcbiAgICAgICAgICAgICAgICAgICAgd2lkdGg6IDQ2cHghaW1wb3J0YW50O1xcclxcbiAgICAgICAgICAgICAgICAgICAgaGVpZ2h0OiA0NnB4IWltcG9ydGFudDtcXHJcXG4gICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQ6ICNmZmYhaW1wb3J0YW50O1xcclxcbiAgICAgICAgICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgIzAwMCFpbXBvcnRhbnQ7XFxyXFxuICAgICAgICAgICAgICAgIH1cXHJcXG4gICAgICAgICAgICB9XFxyXFxuXFxyXFxuICAgICAgICAgICAgQG1peGluIGJsYWNrLXN0b25lcyAoKSB7XFxyXFxuICAgICAgICAgICAgICAgICY6YWZ0ZXIge1xcclxcbiAgICAgICAgICAgICAgICAgICAgbWFyZ2luLXRvcDogLTIzcHghaW1wb3J0YW50O1xcclxcbiAgICAgICAgICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IC0yM3B4IWltcG9ydGFudDtcXHJcXG4gICAgICAgICAgICAgICAgICAgIHdpZHRoOiA0NnB4IWltcG9ydGFudDtcXHJcXG4gICAgICAgICAgICAgICAgICAgIGhlaWdodDogNDZweCFpbXBvcnRhbnQ7XFxyXFxuICAgICAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kOiAjMDAwIWltcG9ydGFudDtcXHJcXG4gICAgICAgICAgICAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICMwMDAhaW1wb3J0YW50O1xcclxcbiAgICAgICAgICAgICAgICB9XFxyXFxuICAgICAgICAgICAgfVxcclxcblxcclxcbiAgICAgICAgICAgICZbZGF0YS14PVxcXCJEXFxcIl1bZGF0YS15PVxcXCI0XFxcIl0sXFxyXFxuICAgICAgICAgICAgJltkYXRhLXg9XFxcIkRcXFwiXVtkYXRhLXk9XFxcIjEyXFxcIl0sXFxyXFxuICAgICAgICAgICAgJltkYXRhLXg9XFxcIkxcXFwiXVtkYXRhLXk9XFxcIjRcXFwiXSxcXHJcXG4gICAgICAgICAgICAmW2RhdGEteD1cXFwiTFxcXCJdW2RhdGEteT1cXFwiMTJcXFwiXSxcXHJcXG4gICAgICAgICAgICAmW2RhdGEteD1cXFwiSFxcXCJdW2RhdGEteT1cXFwiOFxcXCJdIHtcXHJcXG4gICAgICAgICAgICAgICAgLnBpZWNlIHtcXHJcXG4gICAgICAgICAgICAgICAgICAgIEBpbmNsdWRlIHN0YXIoKTtcXHJcXG4gICAgICAgICAgICAgICAgfVxcclxcbiAgICAgICAgICAgIH1cXHJcXG5cXHJcXG4gICAgICAgICAgICAmW2RhdGEtc3RhdHVzPVxcXCJ3aGl0ZVxcXCJde1xcclxcbiAgICAgICAgICAgICAgICAucGllY2Uge1xcclxcbiAgICAgICAgICAgICAgICAgICAgLnBpZWNlX2NvbnRlbnR7XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgY29sb3I6ICMwMDA7XFxyXFxuICAgICAgICAgICAgICAgICAgICB9XFxyXFxuICAgICAgICAgICAgICAgICAgICBAaW5jbHVkZSB3aGl0ZS1zdG9uZXMoKTtcXHJcXG4gICAgICAgICAgICAgICAgfVxcclxcbiAgICAgICAgICAgIH1cXHJcXG5cXHJcXG4gICAgICAgICAgICAmW2RhdGEtc3RhdHVzPVxcXCJibGFja1xcXCJde1xcclxcbiAgICAgICAgICAgICAgICAucGllY2Uge1xcclxcbiAgICAgICAgICAgICAgICAgICAgLnBpZWNlX2NvbnRlbnR7XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgY29sb3I6ICNmZmY7XFxyXFxuICAgICAgICAgICAgICAgICAgICB9XFxyXFxuICAgICAgICAgICAgICAgICAgICBAaW5jbHVkZSBibGFjay1zdG9uZXMoKTtcXHJcXG4gICAgICAgICAgICAgICAgfVxcclxcbiAgICAgICAgICAgIH1cXHJcXG4gICAgICAgIH1cXHJcXG4gICAgfVxcclxcbn1cIl0sXCJzb3VyY2VSb290XCI6XCJcIn1dKTtcblxuLy8gZXhwb3J0c1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvX2Nzcy1sb2FkZXJAMC4yOC43QGNzcy1sb2FkZXI/e1wic291cmNlTWFwXCI6dHJ1ZX0hLi9ub2RlX21vZHVsZXMvX3Nhc3MtbG9hZGVyQDYuMC42QHNhc3MtbG9hZGVyL2xpYi9sb2FkZXIuanM/e1wic291cmNlTWFwXCI6dHJ1ZX0hLi9zcmMvc2Nzcy9pbmRleC5zY3NzXG4vLyBtb2R1bGUgaWQgPSAyXG4vLyBtb2R1bGUgY2h1bmtzID0gMiIsIi8qXG5cdE1JVCBMaWNlbnNlIGh0dHA6Ly93d3cub3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvbWl0LWxpY2Vuc2UucGhwXG5cdEF1dGhvciBUb2JpYXMgS29wcGVycyBAc29rcmFcbiovXG4vLyBjc3MgYmFzZSBjb2RlLCBpbmplY3RlZCBieSB0aGUgY3NzLWxvYWRlclxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbih1c2VTb3VyY2VNYXApIHtcblx0dmFyIGxpc3QgPSBbXTtcblxuXHQvLyByZXR1cm4gdGhlIGxpc3Qgb2YgbW9kdWxlcyBhcyBjc3Mgc3RyaW5nXG5cdGxpc3QudG9TdHJpbmcgPSBmdW5jdGlvbiB0b1N0cmluZygpIHtcblx0XHRyZXR1cm4gdGhpcy5tYXAoZnVuY3Rpb24gKGl0ZW0pIHtcblx0XHRcdHZhciBjb250ZW50ID0gY3NzV2l0aE1hcHBpbmdUb1N0cmluZyhpdGVtLCB1c2VTb3VyY2VNYXApO1xuXHRcdFx0aWYoaXRlbVsyXSkge1xuXHRcdFx0XHRyZXR1cm4gXCJAbWVkaWEgXCIgKyBpdGVtWzJdICsgXCJ7XCIgKyBjb250ZW50ICsgXCJ9XCI7XG5cdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRyZXR1cm4gY29udGVudDtcblx0XHRcdH1cblx0XHR9KS5qb2luKFwiXCIpO1xuXHR9O1xuXG5cdC8vIGltcG9ydCBhIGxpc3Qgb2YgbW9kdWxlcyBpbnRvIHRoZSBsaXN0XG5cdGxpc3QuaSA9IGZ1bmN0aW9uKG1vZHVsZXMsIG1lZGlhUXVlcnkpIHtcblx0XHRpZih0eXBlb2YgbW9kdWxlcyA9PT0gXCJzdHJpbmdcIilcblx0XHRcdG1vZHVsZXMgPSBbW251bGwsIG1vZHVsZXMsIFwiXCJdXTtcblx0XHR2YXIgYWxyZWFkeUltcG9ydGVkTW9kdWxlcyA9IHt9O1xuXHRcdGZvcih2YXIgaSA9IDA7IGkgPCB0aGlzLmxlbmd0aDsgaSsrKSB7XG5cdFx0XHR2YXIgaWQgPSB0aGlzW2ldWzBdO1xuXHRcdFx0aWYodHlwZW9mIGlkID09PSBcIm51bWJlclwiKVxuXHRcdFx0XHRhbHJlYWR5SW1wb3J0ZWRNb2R1bGVzW2lkXSA9IHRydWU7XG5cdFx0fVxuXHRcdGZvcihpID0gMDsgaSA8IG1vZHVsZXMubGVuZ3RoOyBpKyspIHtcblx0XHRcdHZhciBpdGVtID0gbW9kdWxlc1tpXTtcblx0XHRcdC8vIHNraXAgYWxyZWFkeSBpbXBvcnRlZCBtb2R1bGVcblx0XHRcdC8vIHRoaXMgaW1wbGVtZW50YXRpb24gaXMgbm90IDEwMCUgcGVyZmVjdCBmb3Igd2VpcmQgbWVkaWEgcXVlcnkgY29tYmluYXRpb25zXG5cdFx0XHQvLyAgd2hlbiBhIG1vZHVsZSBpcyBpbXBvcnRlZCBtdWx0aXBsZSB0aW1lcyB3aXRoIGRpZmZlcmVudCBtZWRpYSBxdWVyaWVzLlxuXHRcdFx0Ly8gIEkgaG9wZSB0aGlzIHdpbGwgbmV2ZXIgb2NjdXIgKEhleSB0aGlzIHdheSB3ZSBoYXZlIHNtYWxsZXIgYnVuZGxlcylcblx0XHRcdGlmKHR5cGVvZiBpdGVtWzBdICE9PSBcIm51bWJlclwiIHx8ICFhbHJlYWR5SW1wb3J0ZWRNb2R1bGVzW2l0ZW1bMF1dKSB7XG5cdFx0XHRcdGlmKG1lZGlhUXVlcnkgJiYgIWl0ZW1bMl0pIHtcblx0XHRcdFx0XHRpdGVtWzJdID0gbWVkaWFRdWVyeTtcblx0XHRcdFx0fSBlbHNlIGlmKG1lZGlhUXVlcnkpIHtcblx0XHRcdFx0XHRpdGVtWzJdID0gXCIoXCIgKyBpdGVtWzJdICsgXCIpIGFuZCAoXCIgKyBtZWRpYVF1ZXJ5ICsgXCIpXCI7XG5cdFx0XHRcdH1cblx0XHRcdFx0bGlzdC5wdXNoKGl0ZW0pO1xuXHRcdFx0fVxuXHRcdH1cblx0fTtcblx0cmV0dXJuIGxpc3Q7XG59O1xuXG5mdW5jdGlvbiBjc3NXaXRoTWFwcGluZ1RvU3RyaW5nKGl0ZW0sIHVzZVNvdXJjZU1hcCkge1xuXHR2YXIgY29udGVudCA9IGl0ZW1bMV0gfHwgJyc7XG5cdHZhciBjc3NNYXBwaW5nID0gaXRlbVszXTtcblx0aWYgKCFjc3NNYXBwaW5nKSB7XG5cdFx0cmV0dXJuIGNvbnRlbnQ7XG5cdH1cblxuXHRpZiAodXNlU291cmNlTWFwICYmIHR5cGVvZiBidG9hID09PSAnZnVuY3Rpb24nKSB7XG5cdFx0dmFyIHNvdXJjZU1hcHBpbmcgPSB0b0NvbW1lbnQoY3NzTWFwcGluZyk7XG5cdFx0dmFyIHNvdXJjZVVSTHMgPSBjc3NNYXBwaW5nLnNvdXJjZXMubWFwKGZ1bmN0aW9uIChzb3VyY2UpIHtcblx0XHRcdHJldHVybiAnLyojIHNvdXJjZVVSTD0nICsgY3NzTWFwcGluZy5zb3VyY2VSb290ICsgc291cmNlICsgJyAqLydcblx0XHR9KTtcblxuXHRcdHJldHVybiBbY29udGVudF0uY29uY2F0KHNvdXJjZVVSTHMpLmNvbmNhdChbc291cmNlTWFwcGluZ10pLmpvaW4oJ1xcbicpO1xuXHR9XG5cblx0cmV0dXJuIFtjb250ZW50XS5qb2luKCdcXG4nKTtcbn1cblxuLy8gQWRhcHRlZCBmcm9tIGNvbnZlcnQtc291cmNlLW1hcCAoTUlUKVxuZnVuY3Rpb24gdG9Db21tZW50KHNvdXJjZU1hcCkge1xuXHQvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmUgbm8tdW5kZWZcblx0dmFyIGJhc2U2NCA9IGJ0b2EodW5lc2NhcGUoZW5jb2RlVVJJQ29tcG9uZW50KEpTT04uc3RyaW5naWZ5KHNvdXJjZU1hcCkpKSk7XG5cdHZhciBkYXRhID0gJ3NvdXJjZU1hcHBpbmdVUkw9ZGF0YTphcHBsaWNhdGlvbi9qc29uO2NoYXJzZXQ9dXRmLTg7YmFzZTY0LCcgKyBiYXNlNjQ7XG5cblx0cmV0dXJuICcvKiMgJyArIGRhdGEgKyAnICovJztcbn1cblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL19jc3MtbG9hZGVyQDAuMjguN0Bjc3MtbG9hZGVyL2xpYi9jc3MtYmFzZS5qc1xuLy8gbW9kdWxlIGlkID0gM1xuLy8gbW9kdWxlIGNodW5rcyA9IDIiLCIvKlxuXHRNSVQgTGljZW5zZSBodHRwOi8vd3d3Lm9wZW5zb3VyY2Uub3JnL2xpY2Vuc2VzL21pdC1saWNlbnNlLnBocFxuXHRBdXRob3IgVG9iaWFzIEtvcHBlcnMgQHNva3JhXG4qL1xuXG52YXIgc3R5bGVzSW5Eb20gPSB7fTtcblxudmFyXHRtZW1vaXplID0gZnVuY3Rpb24gKGZuKSB7XG5cdHZhciBtZW1vO1xuXG5cdHJldHVybiBmdW5jdGlvbiAoKSB7XG5cdFx0aWYgKHR5cGVvZiBtZW1vID09PSBcInVuZGVmaW5lZFwiKSBtZW1vID0gZm4uYXBwbHkodGhpcywgYXJndW1lbnRzKTtcblx0XHRyZXR1cm4gbWVtbztcblx0fTtcbn07XG5cbnZhciBpc09sZElFID0gbWVtb2l6ZShmdW5jdGlvbiAoKSB7XG5cdC8vIFRlc3QgZm9yIElFIDw9IDkgYXMgcHJvcG9zZWQgYnkgQnJvd3NlcmhhY2tzXG5cdC8vIEBzZWUgaHR0cDovL2Jyb3dzZXJoYWNrcy5jb20vI2hhY2stZTcxZDg2OTJmNjUzMzQxNzNmZWU3MTVjMjIyY2I4MDVcblx0Ly8gVGVzdHMgZm9yIGV4aXN0ZW5jZSBvZiBzdGFuZGFyZCBnbG9iYWxzIGlzIHRvIGFsbG93IHN0eWxlLWxvYWRlclxuXHQvLyB0byBvcGVyYXRlIGNvcnJlY3RseSBpbnRvIG5vbi1zdGFuZGFyZCBlbnZpcm9ubWVudHNcblx0Ly8gQHNlZSBodHRwczovL2dpdGh1Yi5jb20vd2VicGFjay1jb250cmliL3N0eWxlLWxvYWRlci9pc3N1ZXMvMTc3XG5cdHJldHVybiB3aW5kb3cgJiYgZG9jdW1lbnQgJiYgZG9jdW1lbnQuYWxsICYmICF3aW5kb3cuYXRvYjtcbn0pO1xuXG52YXIgZ2V0RWxlbWVudCA9IChmdW5jdGlvbiAoZm4pIHtcblx0dmFyIG1lbW8gPSB7fTtcblxuXHRyZXR1cm4gZnVuY3Rpb24oc2VsZWN0b3IpIHtcblx0XHRpZiAodHlwZW9mIG1lbW9bc2VsZWN0b3JdID09PSBcInVuZGVmaW5lZFwiKSB7XG5cdFx0XHRtZW1vW3NlbGVjdG9yXSA9IGZuLmNhbGwodGhpcywgc2VsZWN0b3IpO1xuXHRcdH1cblxuXHRcdHJldHVybiBtZW1vW3NlbGVjdG9yXVxuXHR9O1xufSkoZnVuY3Rpb24gKHRhcmdldCkge1xuXHRyZXR1cm4gZG9jdW1lbnQucXVlcnlTZWxlY3Rvcih0YXJnZXQpXG59KTtcblxudmFyIHNpbmdsZXRvbiA9IG51bGw7XG52YXJcdHNpbmdsZXRvbkNvdW50ZXIgPSAwO1xudmFyXHRzdHlsZXNJbnNlcnRlZEF0VG9wID0gW107XG5cbnZhclx0Zml4VXJscyA9IHJlcXVpcmUoXCIuL3VybHNcIik7XG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24obGlzdCwgb3B0aW9ucykge1xuXHRpZiAodHlwZW9mIERFQlVHICE9PSBcInVuZGVmaW5lZFwiICYmIERFQlVHKSB7XG5cdFx0aWYgKHR5cGVvZiBkb2N1bWVudCAhPT0gXCJvYmplY3RcIikgdGhyb3cgbmV3IEVycm9yKFwiVGhlIHN0eWxlLWxvYWRlciBjYW5ub3QgYmUgdXNlZCBpbiBhIG5vbi1icm93c2VyIGVudmlyb25tZW50XCIpO1xuXHR9XG5cblx0b3B0aW9ucyA9IG9wdGlvbnMgfHwge307XG5cblx0b3B0aW9ucy5hdHRycyA9IHR5cGVvZiBvcHRpb25zLmF0dHJzID09PSBcIm9iamVjdFwiID8gb3B0aW9ucy5hdHRycyA6IHt9O1xuXG5cdC8vIEZvcmNlIHNpbmdsZS10YWcgc29sdXRpb24gb24gSUU2LTksIHdoaWNoIGhhcyBhIGhhcmQgbGltaXQgb24gdGhlICMgb2YgPHN0eWxlPlxuXHQvLyB0YWdzIGl0IHdpbGwgYWxsb3cgb24gYSBwYWdlXG5cdGlmICghb3B0aW9ucy5zaW5nbGV0b24pIG9wdGlvbnMuc2luZ2xldG9uID0gaXNPbGRJRSgpO1xuXG5cdC8vIEJ5IGRlZmF1bHQsIGFkZCA8c3R5bGU+IHRhZ3MgdG8gdGhlIDxoZWFkPiBlbGVtZW50XG5cdGlmICghb3B0aW9ucy5pbnNlcnRJbnRvKSBvcHRpb25zLmluc2VydEludG8gPSBcImhlYWRcIjtcblxuXHQvLyBCeSBkZWZhdWx0LCBhZGQgPHN0eWxlPiB0YWdzIHRvIHRoZSBib3R0b20gb2YgdGhlIHRhcmdldFxuXHRpZiAoIW9wdGlvbnMuaW5zZXJ0QXQpIG9wdGlvbnMuaW5zZXJ0QXQgPSBcImJvdHRvbVwiO1xuXG5cdHZhciBzdHlsZXMgPSBsaXN0VG9TdHlsZXMobGlzdCwgb3B0aW9ucyk7XG5cblx0YWRkU3R5bGVzVG9Eb20oc3R5bGVzLCBvcHRpb25zKTtcblxuXHRyZXR1cm4gZnVuY3Rpb24gdXBkYXRlIChuZXdMaXN0KSB7XG5cdFx0dmFyIG1heVJlbW92ZSA9IFtdO1xuXG5cdFx0Zm9yICh2YXIgaSA9IDA7IGkgPCBzdHlsZXMubGVuZ3RoOyBpKyspIHtcblx0XHRcdHZhciBpdGVtID0gc3R5bGVzW2ldO1xuXHRcdFx0dmFyIGRvbVN0eWxlID0gc3R5bGVzSW5Eb21baXRlbS5pZF07XG5cblx0XHRcdGRvbVN0eWxlLnJlZnMtLTtcblx0XHRcdG1heVJlbW92ZS5wdXNoKGRvbVN0eWxlKTtcblx0XHR9XG5cblx0XHRpZihuZXdMaXN0KSB7XG5cdFx0XHR2YXIgbmV3U3R5bGVzID0gbGlzdFRvU3R5bGVzKG5ld0xpc3QsIG9wdGlvbnMpO1xuXHRcdFx0YWRkU3R5bGVzVG9Eb20obmV3U3R5bGVzLCBvcHRpb25zKTtcblx0XHR9XG5cblx0XHRmb3IgKHZhciBpID0gMDsgaSA8IG1heVJlbW92ZS5sZW5ndGg7IGkrKykge1xuXHRcdFx0dmFyIGRvbVN0eWxlID0gbWF5UmVtb3ZlW2ldO1xuXG5cdFx0XHRpZihkb21TdHlsZS5yZWZzID09PSAwKSB7XG5cdFx0XHRcdGZvciAodmFyIGogPSAwOyBqIDwgZG9tU3R5bGUucGFydHMubGVuZ3RoOyBqKyspIGRvbVN0eWxlLnBhcnRzW2pdKCk7XG5cblx0XHRcdFx0ZGVsZXRlIHN0eWxlc0luRG9tW2RvbVN0eWxlLmlkXTtcblx0XHRcdH1cblx0XHR9XG5cdH07XG59O1xuXG5mdW5jdGlvbiBhZGRTdHlsZXNUb0RvbSAoc3R5bGVzLCBvcHRpb25zKSB7XG5cdGZvciAodmFyIGkgPSAwOyBpIDwgc3R5bGVzLmxlbmd0aDsgaSsrKSB7XG5cdFx0dmFyIGl0ZW0gPSBzdHlsZXNbaV07XG5cdFx0dmFyIGRvbVN0eWxlID0gc3R5bGVzSW5Eb21baXRlbS5pZF07XG5cblx0XHRpZihkb21TdHlsZSkge1xuXHRcdFx0ZG9tU3R5bGUucmVmcysrO1xuXG5cdFx0XHRmb3IodmFyIGogPSAwOyBqIDwgZG9tU3R5bGUucGFydHMubGVuZ3RoOyBqKyspIHtcblx0XHRcdFx0ZG9tU3R5bGUucGFydHNbal0oaXRlbS5wYXJ0c1tqXSk7XG5cdFx0XHR9XG5cblx0XHRcdGZvcig7IGogPCBpdGVtLnBhcnRzLmxlbmd0aDsgaisrKSB7XG5cdFx0XHRcdGRvbVN0eWxlLnBhcnRzLnB1c2goYWRkU3R5bGUoaXRlbS5wYXJ0c1tqXSwgb3B0aW9ucykpO1xuXHRcdFx0fVxuXHRcdH0gZWxzZSB7XG5cdFx0XHR2YXIgcGFydHMgPSBbXTtcblxuXHRcdFx0Zm9yKHZhciBqID0gMDsgaiA8IGl0ZW0ucGFydHMubGVuZ3RoOyBqKyspIHtcblx0XHRcdFx0cGFydHMucHVzaChhZGRTdHlsZShpdGVtLnBhcnRzW2pdLCBvcHRpb25zKSk7XG5cdFx0XHR9XG5cblx0XHRcdHN0eWxlc0luRG9tW2l0ZW0uaWRdID0ge2lkOiBpdGVtLmlkLCByZWZzOiAxLCBwYXJ0czogcGFydHN9O1xuXHRcdH1cblx0fVxufVxuXG5mdW5jdGlvbiBsaXN0VG9TdHlsZXMgKGxpc3QsIG9wdGlvbnMpIHtcblx0dmFyIHN0eWxlcyA9IFtdO1xuXHR2YXIgbmV3U3R5bGVzID0ge307XG5cblx0Zm9yICh2YXIgaSA9IDA7IGkgPCBsaXN0Lmxlbmd0aDsgaSsrKSB7XG5cdFx0dmFyIGl0ZW0gPSBsaXN0W2ldO1xuXHRcdHZhciBpZCA9IG9wdGlvbnMuYmFzZSA/IGl0ZW1bMF0gKyBvcHRpb25zLmJhc2UgOiBpdGVtWzBdO1xuXHRcdHZhciBjc3MgPSBpdGVtWzFdO1xuXHRcdHZhciBtZWRpYSA9IGl0ZW1bMl07XG5cdFx0dmFyIHNvdXJjZU1hcCA9IGl0ZW1bM107XG5cdFx0dmFyIHBhcnQgPSB7Y3NzOiBjc3MsIG1lZGlhOiBtZWRpYSwgc291cmNlTWFwOiBzb3VyY2VNYXB9O1xuXG5cdFx0aWYoIW5ld1N0eWxlc1tpZF0pIHN0eWxlcy5wdXNoKG5ld1N0eWxlc1tpZF0gPSB7aWQ6IGlkLCBwYXJ0czogW3BhcnRdfSk7XG5cdFx0ZWxzZSBuZXdTdHlsZXNbaWRdLnBhcnRzLnB1c2gocGFydCk7XG5cdH1cblxuXHRyZXR1cm4gc3R5bGVzO1xufVxuXG5mdW5jdGlvbiBpbnNlcnRTdHlsZUVsZW1lbnQgKG9wdGlvbnMsIHN0eWxlKSB7XG5cdHZhciB0YXJnZXQgPSBnZXRFbGVtZW50KG9wdGlvbnMuaW5zZXJ0SW50bylcblxuXHRpZiAoIXRhcmdldCkge1xuXHRcdHRocm93IG5ldyBFcnJvcihcIkNvdWxkbid0IGZpbmQgYSBzdHlsZSB0YXJnZXQuIFRoaXMgcHJvYmFibHkgbWVhbnMgdGhhdCB0aGUgdmFsdWUgZm9yIHRoZSAnaW5zZXJ0SW50bycgcGFyYW1ldGVyIGlzIGludmFsaWQuXCIpO1xuXHR9XG5cblx0dmFyIGxhc3RTdHlsZUVsZW1lbnRJbnNlcnRlZEF0VG9wID0gc3R5bGVzSW5zZXJ0ZWRBdFRvcFtzdHlsZXNJbnNlcnRlZEF0VG9wLmxlbmd0aCAtIDFdO1xuXG5cdGlmIChvcHRpb25zLmluc2VydEF0ID09PSBcInRvcFwiKSB7XG5cdFx0aWYgKCFsYXN0U3R5bGVFbGVtZW50SW5zZXJ0ZWRBdFRvcCkge1xuXHRcdFx0dGFyZ2V0Lmluc2VydEJlZm9yZShzdHlsZSwgdGFyZ2V0LmZpcnN0Q2hpbGQpO1xuXHRcdH0gZWxzZSBpZiAobGFzdFN0eWxlRWxlbWVudEluc2VydGVkQXRUb3AubmV4dFNpYmxpbmcpIHtcblx0XHRcdHRhcmdldC5pbnNlcnRCZWZvcmUoc3R5bGUsIGxhc3RTdHlsZUVsZW1lbnRJbnNlcnRlZEF0VG9wLm5leHRTaWJsaW5nKTtcblx0XHR9IGVsc2Uge1xuXHRcdFx0dGFyZ2V0LmFwcGVuZENoaWxkKHN0eWxlKTtcblx0XHR9XG5cdFx0c3R5bGVzSW5zZXJ0ZWRBdFRvcC5wdXNoKHN0eWxlKTtcblx0fSBlbHNlIGlmIChvcHRpb25zLmluc2VydEF0ID09PSBcImJvdHRvbVwiKSB7XG5cdFx0dGFyZ2V0LmFwcGVuZENoaWxkKHN0eWxlKTtcblx0fSBlbHNlIHtcblx0XHR0aHJvdyBuZXcgRXJyb3IoXCJJbnZhbGlkIHZhbHVlIGZvciBwYXJhbWV0ZXIgJ2luc2VydEF0Jy4gTXVzdCBiZSAndG9wJyBvciAnYm90dG9tJy5cIik7XG5cdH1cbn1cblxuZnVuY3Rpb24gcmVtb3ZlU3R5bGVFbGVtZW50IChzdHlsZSkge1xuXHRpZiAoc3R5bGUucGFyZW50Tm9kZSA9PT0gbnVsbCkgcmV0dXJuIGZhbHNlO1xuXHRzdHlsZS5wYXJlbnROb2RlLnJlbW92ZUNoaWxkKHN0eWxlKTtcblxuXHR2YXIgaWR4ID0gc3R5bGVzSW5zZXJ0ZWRBdFRvcC5pbmRleE9mKHN0eWxlKTtcblx0aWYoaWR4ID49IDApIHtcblx0XHRzdHlsZXNJbnNlcnRlZEF0VG9wLnNwbGljZShpZHgsIDEpO1xuXHR9XG59XG5cbmZ1bmN0aW9uIGNyZWF0ZVN0eWxlRWxlbWVudCAob3B0aW9ucykge1xuXHR2YXIgc3R5bGUgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwic3R5bGVcIik7XG5cblx0b3B0aW9ucy5hdHRycy50eXBlID0gXCJ0ZXh0L2Nzc1wiO1xuXG5cdGFkZEF0dHJzKHN0eWxlLCBvcHRpb25zLmF0dHJzKTtcblx0aW5zZXJ0U3R5bGVFbGVtZW50KG9wdGlvbnMsIHN0eWxlKTtcblxuXHRyZXR1cm4gc3R5bGU7XG59XG5cbmZ1bmN0aW9uIGNyZWF0ZUxpbmtFbGVtZW50IChvcHRpb25zKSB7XG5cdHZhciBsaW5rID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcImxpbmtcIik7XG5cblx0b3B0aW9ucy5hdHRycy50eXBlID0gXCJ0ZXh0L2Nzc1wiO1xuXHRvcHRpb25zLmF0dHJzLnJlbCA9IFwic3R5bGVzaGVldFwiO1xuXG5cdGFkZEF0dHJzKGxpbmssIG9wdGlvbnMuYXR0cnMpO1xuXHRpbnNlcnRTdHlsZUVsZW1lbnQob3B0aW9ucywgbGluayk7XG5cblx0cmV0dXJuIGxpbms7XG59XG5cbmZ1bmN0aW9uIGFkZEF0dHJzIChlbCwgYXR0cnMpIHtcblx0T2JqZWN0LmtleXMoYXR0cnMpLmZvckVhY2goZnVuY3Rpb24gKGtleSkge1xuXHRcdGVsLnNldEF0dHJpYnV0ZShrZXksIGF0dHJzW2tleV0pO1xuXHR9KTtcbn1cblxuZnVuY3Rpb24gYWRkU3R5bGUgKG9iaiwgb3B0aW9ucykge1xuXHR2YXIgc3R5bGUsIHVwZGF0ZSwgcmVtb3ZlLCByZXN1bHQ7XG5cblx0Ly8gSWYgYSB0cmFuc2Zvcm0gZnVuY3Rpb24gd2FzIGRlZmluZWQsIHJ1biBpdCBvbiB0aGUgY3NzXG5cdGlmIChvcHRpb25zLnRyYW5zZm9ybSAmJiBvYmouY3NzKSB7XG5cdCAgICByZXN1bHQgPSBvcHRpb25zLnRyYW5zZm9ybShvYmouY3NzKTtcblxuXHQgICAgaWYgKHJlc3VsdCkge1xuXHQgICAgXHQvLyBJZiB0cmFuc2Zvcm0gcmV0dXJucyBhIHZhbHVlLCB1c2UgdGhhdCBpbnN0ZWFkIG9mIHRoZSBvcmlnaW5hbCBjc3MuXG5cdCAgICBcdC8vIFRoaXMgYWxsb3dzIHJ1bm5pbmcgcnVudGltZSB0cmFuc2Zvcm1hdGlvbnMgb24gdGhlIGNzcy5cblx0ICAgIFx0b2JqLmNzcyA9IHJlc3VsdDtcblx0ICAgIH0gZWxzZSB7XG5cdCAgICBcdC8vIElmIHRoZSB0cmFuc2Zvcm0gZnVuY3Rpb24gcmV0dXJucyBhIGZhbHN5IHZhbHVlLCBkb24ndCBhZGQgdGhpcyBjc3MuXG5cdCAgICBcdC8vIFRoaXMgYWxsb3dzIGNvbmRpdGlvbmFsIGxvYWRpbmcgb2YgY3NzXG5cdCAgICBcdHJldHVybiBmdW5jdGlvbigpIHtcblx0ICAgIFx0XHQvLyBub29wXG5cdCAgICBcdH07XG5cdCAgICB9XG5cdH1cblxuXHRpZiAob3B0aW9ucy5zaW5nbGV0b24pIHtcblx0XHR2YXIgc3R5bGVJbmRleCA9IHNpbmdsZXRvbkNvdW50ZXIrKztcblxuXHRcdHN0eWxlID0gc2luZ2xldG9uIHx8IChzaW5nbGV0b24gPSBjcmVhdGVTdHlsZUVsZW1lbnQob3B0aW9ucykpO1xuXG5cdFx0dXBkYXRlID0gYXBwbHlUb1NpbmdsZXRvblRhZy5iaW5kKG51bGwsIHN0eWxlLCBzdHlsZUluZGV4LCBmYWxzZSk7XG5cdFx0cmVtb3ZlID0gYXBwbHlUb1NpbmdsZXRvblRhZy5iaW5kKG51bGwsIHN0eWxlLCBzdHlsZUluZGV4LCB0cnVlKTtcblxuXHR9IGVsc2UgaWYgKFxuXHRcdG9iai5zb3VyY2VNYXAgJiZcblx0XHR0eXBlb2YgVVJMID09PSBcImZ1bmN0aW9uXCIgJiZcblx0XHR0eXBlb2YgVVJMLmNyZWF0ZU9iamVjdFVSTCA9PT0gXCJmdW5jdGlvblwiICYmXG5cdFx0dHlwZW9mIFVSTC5yZXZva2VPYmplY3RVUkwgPT09IFwiZnVuY3Rpb25cIiAmJlxuXHRcdHR5cGVvZiBCbG9iID09PSBcImZ1bmN0aW9uXCIgJiZcblx0XHR0eXBlb2YgYnRvYSA9PT0gXCJmdW5jdGlvblwiXG5cdCkge1xuXHRcdHN0eWxlID0gY3JlYXRlTGlua0VsZW1lbnQob3B0aW9ucyk7XG5cdFx0dXBkYXRlID0gdXBkYXRlTGluay5iaW5kKG51bGwsIHN0eWxlLCBvcHRpb25zKTtcblx0XHRyZW1vdmUgPSBmdW5jdGlvbiAoKSB7XG5cdFx0XHRyZW1vdmVTdHlsZUVsZW1lbnQoc3R5bGUpO1xuXG5cdFx0XHRpZihzdHlsZS5ocmVmKSBVUkwucmV2b2tlT2JqZWN0VVJMKHN0eWxlLmhyZWYpO1xuXHRcdH07XG5cdH0gZWxzZSB7XG5cdFx0c3R5bGUgPSBjcmVhdGVTdHlsZUVsZW1lbnQob3B0aW9ucyk7XG5cdFx0dXBkYXRlID0gYXBwbHlUb1RhZy5iaW5kKG51bGwsIHN0eWxlKTtcblx0XHRyZW1vdmUgPSBmdW5jdGlvbiAoKSB7XG5cdFx0XHRyZW1vdmVTdHlsZUVsZW1lbnQoc3R5bGUpO1xuXHRcdH07XG5cdH1cblxuXHR1cGRhdGUob2JqKTtcblxuXHRyZXR1cm4gZnVuY3Rpb24gdXBkYXRlU3R5bGUgKG5ld09iaikge1xuXHRcdGlmIChuZXdPYmopIHtcblx0XHRcdGlmIChcblx0XHRcdFx0bmV3T2JqLmNzcyA9PT0gb2JqLmNzcyAmJlxuXHRcdFx0XHRuZXdPYmoubWVkaWEgPT09IG9iai5tZWRpYSAmJlxuXHRcdFx0XHRuZXdPYmouc291cmNlTWFwID09PSBvYmouc291cmNlTWFwXG5cdFx0XHQpIHtcblx0XHRcdFx0cmV0dXJuO1xuXHRcdFx0fVxuXG5cdFx0XHR1cGRhdGUob2JqID0gbmV3T2JqKTtcblx0XHR9IGVsc2Uge1xuXHRcdFx0cmVtb3ZlKCk7XG5cdFx0fVxuXHR9O1xufVxuXG52YXIgcmVwbGFjZVRleHQgPSAoZnVuY3Rpb24gKCkge1xuXHR2YXIgdGV4dFN0b3JlID0gW107XG5cblx0cmV0dXJuIGZ1bmN0aW9uIChpbmRleCwgcmVwbGFjZW1lbnQpIHtcblx0XHR0ZXh0U3RvcmVbaW5kZXhdID0gcmVwbGFjZW1lbnQ7XG5cblx0XHRyZXR1cm4gdGV4dFN0b3JlLmZpbHRlcihCb29sZWFuKS5qb2luKCdcXG4nKTtcblx0fTtcbn0pKCk7XG5cbmZ1bmN0aW9uIGFwcGx5VG9TaW5nbGV0b25UYWcgKHN0eWxlLCBpbmRleCwgcmVtb3ZlLCBvYmopIHtcblx0dmFyIGNzcyA9IHJlbW92ZSA/IFwiXCIgOiBvYmouY3NzO1xuXG5cdGlmIChzdHlsZS5zdHlsZVNoZWV0KSB7XG5cdFx0c3R5bGUuc3R5bGVTaGVldC5jc3NUZXh0ID0gcmVwbGFjZVRleHQoaW5kZXgsIGNzcyk7XG5cdH0gZWxzZSB7XG5cdFx0dmFyIGNzc05vZGUgPSBkb2N1bWVudC5jcmVhdGVUZXh0Tm9kZShjc3MpO1xuXHRcdHZhciBjaGlsZE5vZGVzID0gc3R5bGUuY2hpbGROb2RlcztcblxuXHRcdGlmIChjaGlsZE5vZGVzW2luZGV4XSkgc3R5bGUucmVtb3ZlQ2hpbGQoY2hpbGROb2Rlc1tpbmRleF0pO1xuXG5cdFx0aWYgKGNoaWxkTm9kZXMubGVuZ3RoKSB7XG5cdFx0XHRzdHlsZS5pbnNlcnRCZWZvcmUoY3NzTm9kZSwgY2hpbGROb2Rlc1tpbmRleF0pO1xuXHRcdH0gZWxzZSB7XG5cdFx0XHRzdHlsZS5hcHBlbmRDaGlsZChjc3NOb2RlKTtcblx0XHR9XG5cdH1cbn1cblxuZnVuY3Rpb24gYXBwbHlUb1RhZyAoc3R5bGUsIG9iaikge1xuXHR2YXIgY3NzID0gb2JqLmNzcztcblx0dmFyIG1lZGlhID0gb2JqLm1lZGlhO1xuXG5cdGlmKG1lZGlhKSB7XG5cdFx0c3R5bGUuc2V0QXR0cmlidXRlKFwibWVkaWFcIiwgbWVkaWEpXG5cdH1cblxuXHRpZihzdHlsZS5zdHlsZVNoZWV0KSB7XG5cdFx0c3R5bGUuc3R5bGVTaGVldC5jc3NUZXh0ID0gY3NzO1xuXHR9IGVsc2Uge1xuXHRcdHdoaWxlKHN0eWxlLmZpcnN0Q2hpbGQpIHtcblx0XHRcdHN0eWxlLnJlbW92ZUNoaWxkKHN0eWxlLmZpcnN0Q2hpbGQpO1xuXHRcdH1cblxuXHRcdHN0eWxlLmFwcGVuZENoaWxkKGRvY3VtZW50LmNyZWF0ZVRleHROb2RlKGNzcykpO1xuXHR9XG59XG5cbmZ1bmN0aW9uIHVwZGF0ZUxpbmsgKGxpbmssIG9wdGlvbnMsIG9iaikge1xuXHR2YXIgY3NzID0gb2JqLmNzcztcblx0dmFyIHNvdXJjZU1hcCA9IG9iai5zb3VyY2VNYXA7XG5cblx0Lypcblx0XHRJZiBjb252ZXJ0VG9BYnNvbHV0ZVVybHMgaXNuJ3QgZGVmaW5lZCwgYnV0IHNvdXJjZW1hcHMgYXJlIGVuYWJsZWRcblx0XHRhbmQgdGhlcmUgaXMgbm8gcHVibGljUGF0aCBkZWZpbmVkIHRoZW4gbGV0cyB0dXJuIGNvbnZlcnRUb0Fic29sdXRlVXJsc1xuXHRcdG9uIGJ5IGRlZmF1bHQuICBPdGhlcndpc2UgZGVmYXVsdCB0byB0aGUgY29udmVydFRvQWJzb2x1dGVVcmxzIG9wdGlvblxuXHRcdGRpcmVjdGx5XG5cdCovXG5cdHZhciBhdXRvRml4VXJscyA9IG9wdGlvbnMuY29udmVydFRvQWJzb2x1dGVVcmxzID09PSB1bmRlZmluZWQgJiYgc291cmNlTWFwO1xuXG5cdGlmIChvcHRpb25zLmNvbnZlcnRUb0Fic29sdXRlVXJscyB8fCBhdXRvRml4VXJscykge1xuXHRcdGNzcyA9IGZpeFVybHMoY3NzKTtcblx0fVxuXG5cdGlmIChzb3VyY2VNYXApIHtcblx0XHQvLyBodHRwOi8vc3RhY2tvdmVyZmxvdy5jb20vYS8yNjYwMzg3NVxuXHRcdGNzcyArPSBcIlxcbi8qIyBzb3VyY2VNYXBwaW5nVVJMPWRhdGE6YXBwbGljYXRpb24vanNvbjtiYXNlNjQsXCIgKyBidG9hKHVuZXNjYXBlKGVuY29kZVVSSUNvbXBvbmVudChKU09OLnN0cmluZ2lmeShzb3VyY2VNYXApKSkpICsgXCIgKi9cIjtcblx0fVxuXG5cdHZhciBibG9iID0gbmV3IEJsb2IoW2Nzc10sIHsgdHlwZTogXCJ0ZXh0L2Nzc1wiIH0pO1xuXG5cdHZhciBvbGRTcmMgPSBsaW5rLmhyZWY7XG5cblx0bGluay5ocmVmID0gVVJMLmNyZWF0ZU9iamVjdFVSTChibG9iKTtcblxuXHRpZihvbGRTcmMpIFVSTC5yZXZva2VPYmplY3RVUkwob2xkU3JjKTtcbn1cblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL19zdHlsZS1sb2FkZXJAMC4xOC4yQHN0eWxlLWxvYWRlci9saWIvYWRkU3R5bGVzLmpzXG4vLyBtb2R1bGUgaWQgPSA0XG4vLyBtb2R1bGUgY2h1bmtzID0gMiIsIlxuLyoqXG4gKiBXaGVuIHNvdXJjZSBtYXBzIGFyZSBlbmFibGVkLCBgc3R5bGUtbG9hZGVyYCB1c2VzIGEgbGluayBlbGVtZW50IHdpdGggYSBkYXRhLXVyaSB0b1xuICogZW1iZWQgdGhlIGNzcyBvbiB0aGUgcGFnZS4gVGhpcyBicmVha3MgYWxsIHJlbGF0aXZlIHVybHMgYmVjYXVzZSBub3cgdGhleSBhcmUgcmVsYXRpdmUgdG8gYVxuICogYnVuZGxlIGluc3RlYWQgb2YgdGhlIGN1cnJlbnQgcGFnZS5cbiAqXG4gKiBPbmUgc29sdXRpb24gaXMgdG8gb25seSB1c2UgZnVsbCB1cmxzLCBidXQgdGhhdCBtYXkgYmUgaW1wb3NzaWJsZS5cbiAqXG4gKiBJbnN0ZWFkLCB0aGlzIGZ1bmN0aW9uIFwiZml4ZXNcIiB0aGUgcmVsYXRpdmUgdXJscyB0byBiZSBhYnNvbHV0ZSBhY2NvcmRpbmcgdG8gdGhlIGN1cnJlbnQgcGFnZSBsb2NhdGlvbi5cbiAqXG4gKiBBIHJ1ZGltZW50YXJ5IHRlc3Qgc3VpdGUgaXMgbG9jYXRlZCBhdCBgdGVzdC9maXhVcmxzLmpzYCBhbmQgY2FuIGJlIHJ1biB2aWEgdGhlIGBucG0gdGVzdGAgY29tbWFuZC5cbiAqXG4gKi9cblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAoY3NzKSB7XG4gIC8vIGdldCBjdXJyZW50IGxvY2F0aW9uXG4gIHZhciBsb2NhdGlvbiA9IHR5cGVvZiB3aW5kb3cgIT09IFwidW5kZWZpbmVkXCIgJiYgd2luZG93LmxvY2F0aW9uO1xuXG4gIGlmICghbG9jYXRpb24pIHtcbiAgICB0aHJvdyBuZXcgRXJyb3IoXCJmaXhVcmxzIHJlcXVpcmVzIHdpbmRvdy5sb2NhdGlvblwiKTtcbiAgfVxuXG5cdC8vIGJsYW5rIG9yIG51bGw/XG5cdGlmICghY3NzIHx8IHR5cGVvZiBjc3MgIT09IFwic3RyaW5nXCIpIHtcblx0ICByZXR1cm4gY3NzO1xuICB9XG5cbiAgdmFyIGJhc2VVcmwgPSBsb2NhdGlvbi5wcm90b2NvbCArIFwiLy9cIiArIGxvY2F0aW9uLmhvc3Q7XG4gIHZhciBjdXJyZW50RGlyID0gYmFzZVVybCArIGxvY2F0aW9uLnBhdGhuYW1lLnJlcGxhY2UoL1xcL1teXFwvXSokLywgXCIvXCIpO1xuXG5cdC8vIGNvbnZlcnQgZWFjaCB1cmwoLi4uKVxuXHQvKlxuXHRUaGlzIHJlZ3VsYXIgZXhwcmVzc2lvbiBpcyBqdXN0IGEgd2F5IHRvIHJlY3Vyc2l2ZWx5IG1hdGNoIGJyYWNrZXRzIHdpdGhpblxuXHRhIHN0cmluZy5cblxuXHQgL3VybFxccypcXCggID0gTWF0Y2ggb24gdGhlIHdvcmQgXCJ1cmxcIiB3aXRoIGFueSB3aGl0ZXNwYWNlIGFmdGVyIGl0IGFuZCB0aGVuIGEgcGFyZW5zXG5cdCAgICggID0gU3RhcnQgYSBjYXB0dXJpbmcgZ3JvdXBcblx0ICAgICAoPzogID0gU3RhcnQgYSBub24tY2FwdHVyaW5nIGdyb3VwXG5cdCAgICAgICAgIFteKShdICA9IE1hdGNoIGFueXRoaW5nIHRoYXQgaXNuJ3QgYSBwYXJlbnRoZXNlc1xuXHQgICAgICAgICB8ICA9IE9SXG5cdCAgICAgICAgIFxcKCAgPSBNYXRjaCBhIHN0YXJ0IHBhcmVudGhlc2VzXG5cdCAgICAgICAgICAgICAoPzogID0gU3RhcnQgYW5vdGhlciBub24tY2FwdHVyaW5nIGdyb3Vwc1xuXHQgICAgICAgICAgICAgICAgIFteKShdKyAgPSBNYXRjaCBhbnl0aGluZyB0aGF0IGlzbid0IGEgcGFyZW50aGVzZXNcblx0ICAgICAgICAgICAgICAgICB8ICA9IE9SXG5cdCAgICAgICAgICAgICAgICAgXFwoICA9IE1hdGNoIGEgc3RhcnQgcGFyZW50aGVzZXNcblx0ICAgICAgICAgICAgICAgICAgICAgW14pKF0qICA9IE1hdGNoIGFueXRoaW5nIHRoYXQgaXNuJ3QgYSBwYXJlbnRoZXNlc1xuXHQgICAgICAgICAgICAgICAgIFxcKSAgPSBNYXRjaCBhIGVuZCBwYXJlbnRoZXNlc1xuXHQgICAgICAgICAgICAgKSAgPSBFbmQgR3JvdXBcbiAgICAgICAgICAgICAgKlxcKSA9IE1hdGNoIGFueXRoaW5nIGFuZCB0aGVuIGEgY2xvc2UgcGFyZW5zXG4gICAgICAgICAgKSAgPSBDbG9zZSBub24tY2FwdHVyaW5nIGdyb3VwXG4gICAgICAgICAgKiAgPSBNYXRjaCBhbnl0aGluZ1xuICAgICAgICkgID0gQ2xvc2UgY2FwdHVyaW5nIGdyb3VwXG5cdCBcXCkgID0gTWF0Y2ggYSBjbG9zZSBwYXJlbnNcblxuXHQgL2dpICA9IEdldCBhbGwgbWF0Y2hlcywgbm90IHRoZSBmaXJzdC4gIEJlIGNhc2UgaW5zZW5zaXRpdmUuXG5cdCAqL1xuXHR2YXIgZml4ZWRDc3MgPSBjc3MucmVwbGFjZSgvdXJsXFxzKlxcKCgoPzpbXikoXXxcXCgoPzpbXikoXSt8XFwoW14pKF0qXFwpKSpcXCkpKilcXCkvZ2ksIGZ1bmN0aW9uKGZ1bGxNYXRjaCwgb3JpZ1VybCkge1xuXHRcdC8vIHN0cmlwIHF1b3RlcyAoaWYgdGhleSBleGlzdClcblx0XHR2YXIgdW5xdW90ZWRPcmlnVXJsID0gb3JpZ1VybFxuXHRcdFx0LnRyaW0oKVxuXHRcdFx0LnJlcGxhY2UoL15cIiguKilcIiQvLCBmdW5jdGlvbihvLCAkMSl7IHJldHVybiAkMTsgfSlcblx0XHRcdC5yZXBsYWNlKC9eJyguKiknJC8sIGZ1bmN0aW9uKG8sICQxKXsgcmV0dXJuICQxOyB9KTtcblxuXHRcdC8vIGFscmVhZHkgYSBmdWxsIHVybD8gbm8gY2hhbmdlXG5cdFx0aWYgKC9eKCN8ZGF0YTp8aHR0cDpcXC9cXC98aHR0cHM6XFwvXFwvfGZpbGU6XFwvXFwvXFwvKS9pLnRlc3QodW5xdW90ZWRPcmlnVXJsKSkge1xuXHRcdCAgcmV0dXJuIGZ1bGxNYXRjaDtcblx0XHR9XG5cblx0XHQvLyBjb252ZXJ0IHRoZSB1cmwgdG8gYSBmdWxsIHVybFxuXHRcdHZhciBuZXdVcmw7XG5cblx0XHRpZiAodW5xdW90ZWRPcmlnVXJsLmluZGV4T2YoXCIvL1wiKSA9PT0gMCkge1xuXHRcdCAgXHQvL1RPRE86IHNob3VsZCB3ZSBhZGQgcHJvdG9jb2w/XG5cdFx0XHRuZXdVcmwgPSB1bnF1b3RlZE9yaWdVcmw7XG5cdFx0fSBlbHNlIGlmICh1bnF1b3RlZE9yaWdVcmwuaW5kZXhPZihcIi9cIikgPT09IDApIHtcblx0XHRcdC8vIHBhdGggc2hvdWxkIGJlIHJlbGF0aXZlIHRvIHRoZSBiYXNlIHVybFxuXHRcdFx0bmV3VXJsID0gYmFzZVVybCArIHVucXVvdGVkT3JpZ1VybDsgLy8gYWxyZWFkeSBzdGFydHMgd2l0aCAnLydcblx0XHR9IGVsc2Uge1xuXHRcdFx0Ly8gcGF0aCBzaG91bGQgYmUgcmVsYXRpdmUgdG8gY3VycmVudCBkaXJlY3Rvcnlcblx0XHRcdG5ld1VybCA9IGN1cnJlbnREaXIgKyB1bnF1b3RlZE9yaWdVcmwucmVwbGFjZSgvXlxcLlxcLy8sIFwiXCIpOyAvLyBTdHJpcCBsZWFkaW5nICcuLydcblx0XHR9XG5cblx0XHQvLyBzZW5kIGJhY2sgdGhlIGZpeGVkIHVybCguLi4pXG5cdFx0cmV0dXJuIFwidXJsKFwiICsgSlNPTi5zdHJpbmdpZnkobmV3VXJsKSArIFwiKVwiO1xuXHR9KTtcblxuXHQvLyBzZW5kIGJhY2sgdGhlIGZpeGVkIGNzc1xuXHRyZXR1cm4gZml4ZWRDc3M7XG59O1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvX3N0eWxlLWxvYWRlckAwLjE4LjJAc3R5bGUtbG9hZGVyL2xpYi91cmxzLmpzXG4vLyBtb2R1bGUgaWQgPSA1XG4vLyBtb2R1bGUgY2h1bmtzID0gMiIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZS5lbnN1cmUoW10sIHJlcXVpcmUgPT4gcmVxdWlyZSgnbG9kYXNoJyksICdsaWIvbG9kYXNoJylcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3NyYy9saWIvbG9kYXNoLmpzXG4vLyBtb2R1bGUgaWQgPSA2XG4vLyBtb2R1bGUgY2h1bmtzID0gMiIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZS5lbnN1cmUoW10sIHJlcXVpcmUgPT4gcmVxdWlyZSgnLi9tYWluLmpzJyksICdjb21wb25lbnRzL2FwbGhhYmV0JylcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL3NyYy9jb21wb25lbnRzL2FscGhhYmV0L2luZGV4LmpzXG4vLyBtb2R1bGUgaWQgPSA3XG4vLyBtb2R1bGUgY2h1bmtzID0gMiJdLCJzb3VyY2VSb290IjoiIn0=