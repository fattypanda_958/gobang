let list = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
module.exports = {
    alphabet_encode ( v) {
        return list[v-1];
    },
    alphabet_decode ( v) {
        return list.indexOf(v) + 1;
    }
}